#!/usr/bin/env python3

# https://cryptography.io/en/latest/hazmat/primitives/asymmetric/ed25519.html

import sys

from base64 import b64decode, b64encode

from cryptography.hazmat.primitives.asymmetric import ec, rsa
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives import serialization

key = load_pem_private_key(
    bytes(sys.stdin.read(), encoding='utf-8'),
    password=None,
)

print(type(key))
if isinstance(key, rsa.RSAPrivateKey):
    print("RSA")
elif isinstance(key, ec.EllipticCurvePrivateKey):
    print("ed25519")

public_key = key.public_key().public_bytes(
    encoding=serialization.Encoding.Raw,
    format=serialization.PublicFormat.Raw
)

print("Public key:", b64encode(public_key).decode('utf-8'))

sys.exit()

