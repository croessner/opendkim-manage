#!/usr/bin/env python3

# https://cryptography.io/en/latest/hazmat/primitives/asymmetric/ed25519.html

from base64 import b64encode
from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PrivateKey
from cryptography.hazmat.primitives import serialization

# Generate ed25519 private key
private_key = Ed25519PrivateKey.generate()

# Get public key
public_key = private_key.public_key()

raw_private = private_key.private_bytes(
    encoding=serialization.Encoding.Raw,
    format=serialization.PrivateFormat.Raw,
    encryption_algorithm=serialization.NoEncryption()
)
print("Private raw key b64", b64encode(raw_private).decode('utf-8'))

pem_private = private_key.private_bytes(
    encoding=serialization.Encoding.PEM,
    format=serialization.PrivateFormat.PKCS8,
    encryption_algorithm=serialization.NoEncryption()
)
print("Private key PEM\n" + str(pem_private, encoding='utf-8'))

raw_public = public_key.public_bytes(
    encoding=serialization.Encoding.Raw,
    format=serialization.PublicFormat.Raw
)
print("Public raw key b64", b64encode(raw_public).decode('utf-8'))

print("Public and private b64", b64encode(raw_public + raw_private).decode('utf-8'))

