# Document history

Version 1.0, 28.12.2020, Author Christian Rößner

# Overview

OpenDKIM is used to sign e-mail messages. It is able to fetch DKIM private keys
from an LDAP Server. *dkimmanager.py* is intended to write such private keys
into an LDAP server. If your OpenDKIM box has direct access to a primary DNS
server, *dkimmanger.py* can also do dynamic zone updates. Only one DNS server is
currently supported.

## Features

* Create RSA and ed25519 keys in LDAP (If you decide to use ed25519 keys, 
  you will need to use OpenDKIM from the [develop](https://github.com/trusteddomainproject/OpenDKIM.git) branch)
* Synchronize LDAP data with a DNS server
* DNS server support with and without TSIG keys
* Support for revoked keys
* Support for CNAMEs, if customers manage DNS entries themselfs

This implementation follows the recommendations from [m3aawg-dkim-key-rotation-bp-2019-03.pdf](https://www.m3aawg.org/sites/default/files/m3aawg-dkim-key-rotation-bp-2019-03.pdf)

## Prerequirements

This script requires Python 3.9 or higher. It will work on future Debian
(Bullseye), which already supports Python 3.9. Another way is to use the
provided Dockerfile as a template.

If you want to install the Python dependencies, make sure you have developer
libraries for OpenLDAP and Cyrus-SASL installed.

Example for Debian:

```
apt install libldap2-dev
apt install libsasl2-dev
apt install python3-dev
apt install python3-setuptools
```

## Installation

You can simply call:

```
pip install opendkim-manage
```

to install the script and its dependencies.

*dkimmanager.py* is a tool to mange DKIM keys stored in OpenLDAP. It uses a
modified version of the OpenDKIM LDAP schema, which is also shipped with this
release. You can find the schema in the contrib folder as well as a sample 
configuration file. The latter is commented and should give enough
information to get a successful start.

You can call the script as follows to get a quick overview about all common
options:

```
dkimmanager.py --help
usage: opendkim-manage [-h] [--list] [--create] [--delete] [--force-delete] [--active] [--force-active] [--age AGE]
                       [--domain DOMAIN] [--selectorname SELECTORNAME] [--size SIZE] [--keytype {both,rsa,ed25519}]
                       [--testkey] [--config CONFIG] [--add-missing] [--max-initial MAX_INITIAL]
                       [--max-revoked MAX_REVOKED] [--add-new] [--rotate] [--auto] [--print-dns]
                       [--expire-after EXPIRE_AFTER] [--delete-delay DELETE_DELAY] [--update-dns] [--interactive] [--debug]
                       [--verbose] [--color] [--version]

optional arguments:
  -h, --help            show this help message and exit
  --list, -l            List DKIM keys
  --create, -c          Create a new DKIM key
  --delete, -d          Delete one or many DKIM keys
  --force-delete        Force deletion of a DKIM key
  --active              Set DKIMActive to TRUE for a selector
  --force-active        Force activation of a DKIM key
  --age AGE, -A AGE     The key has to be more(+) or less (-) then n days old
  --domain DOMAIN, -D DOMAIN
                        A DNS domain name
  --selectorname SELECTORNAME, -s SELECTORNAME
                        A selector name
  --size SIZE, -S SIZE  Size of DKIM keys (default: 2048)
  --keytype {both,rsa,ed25519}, -k {both,rsa,ed25519}
  --testkey, -t         Check that the listed DKIM keys are published and useable
  --config CONFIG, -f CONFIG
                        Path to 'opendkim-manage' config file. (default: '/etc/opendkim-manage.cfg'
  --add-missing, -m     Add missing DKIM keys to LDAP objects
  --max-initial MAX_INITIAL
                        Maximum number of newly created DKIM keys
  --max-revoked MAX_REVOKED, -R MAX_REVOKED
                        Maximum number of revoked DKIM keys that shell be kept
  --add-new, -n         Check age for DKIM keys and create new keys on demand
  --rotate, -r          Rotate one or all DKIM keys
  --auto, -a            Short for --add-missing, --add-new, --rotate and --delete
  --print-dns           Print out the public DNS information for a selector name
  --expire-after EXPIRE_AFTER, -e EXPIRE_AFTER
                        Number of days after which new DKIM keys will be created with --add-new (default: 365 days)
  --delete-delay DELETE_DELAY, -y DELETE_DELAY
                        Delay deletion of old DKIM keys (default: 10 days)
  --update-dns, -u      Update DNS zones
  --interactive, -i     Turn on interactive mode
  --debug               Turn on debugging
  --verbose, -v         Verbose output
  --color               Turn on colors for output
  --version, -V         Print version and exit
```

Most important options are *--auto* and *--update-dns*. If you want to get
some output, you can add *--verbose*.

If you have run the script, you can run it again with the main option
*--list*. This will show you the current state of your LDAP stored DKIM keys.

This is a real live example:
```
dkimmanager.py -f /etc/opendkim-manage.cfg --list
DNS domain 'exampleserver.de':
DN: dc=exampleserver,dc=de,ou=dkim,ou=it,dc=roessner-net,dc=de
2020-12-11 14:49:53 DKIMSelector: s90A4168E1A-7A605A-B2CF2A-2020-12 DKIMKeyType.RSA [revoked]
2020-12-11 14:50:01 DKIMSelector: s09792C0E2A-9A99C1-7CB471-2020-12 DKIMKeyType.ED25519 [revoked]
2020-12-14 09:37:07 DKIMSelector: s74C5474208-850319-3C38FF-2020-12 DKIMKeyType.RSA [revoked]
2020-12-14 09:37:08 DKIMSelector: s3272320184-2F62CE-AFB266-2020-12 DKIMKeyType.ED25519 [revoked]
2020-12-16 10:11:13 DKIMSelector: sA39D65AB3E-2020-12 DKIMKeyType.RSA (active)
2020-12-16 10:11:13 DKIMSelector: s353A40CAC8-2020-12 DKIMKeyType.ED25519 (active)
DNS domain 'mlserv.org':
DN: dc=mlserv,dc=org,ou=dkim,ou=it,dc=roessner-net,dc=de
2020-12-16 12:06:31 DKIMSelector: default-rsa-1 DKIMKeyType.RSA (active)
2020-12-16 12:06:31 DKIMSelector: default-ed25519-1 DKIMKeyType.ED25519 (active)
```

If you have tested the functionaility, you may add a cron job at the end that
runs once a day:
 
```cron
@daily /usr/local/sbin/opendkim-manage --auto --verbose --update-dns
```

If you find issues, you are welcome to use the issue tracker here at Github 
or send a pull request. Have fun...

## LDAP-structure example

```
dn: dc=acme,dc=com
objectClass: top
objectClass: dcObject
objectClass: organization
o: Example Inc.
dc: acme

dn: ou=dkim,dc=acme,dc=com
ou: dkim
objectClass: top
objectClass: organizationalUnit

dn: dc=de,ou=dkim,dc=acme,dc=com
dc: de
objectClass: domain
objectClass: top

dn: dc=exampleserver,dc=de,ou=dkim,dc=acme,dc=com
objectClass: domain
objectClass: top
objectClass: domainRelatedObject
dc: exampleserver
associatedDomain: exampleserver.de
```

If you plan on using the cname-feature, you will need to have objects like
the following one in LDAP:

```
dn: dc=foo,dc=de,ou=dkim,dc=acme,dc=com
objectClass: domain
objectClass: top
objectClass: domainRelatedObject
dc: foo
associatedDomain: foo.de
destinationIndicator: cnames.example.test
```

The *destinationIndicator* value is a place in DNS, where *dkimmanager.py* will
create static keys that can be published to customers. You need this method, if
you are not responsible for the DNS server for such domains. A customer can
place CNAMEs in his zone to the ones under ...cnames.example.test. See the
example configuration file for more details.

This is enough for starting. The LDAP user given in the configuration file
must have enough rights to write into the directory.
