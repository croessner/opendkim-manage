from setuptools import setup, find_packages


with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name='opendkim-manage',
    version='2020.01',
    keywords='OpenDKIM OpenLDAP DKIM key management',
    packages=find_packages(),
    entry_points={
        'console_scripts': ['opendkim-manage=dkimmanager.dkimmanager:main']
    },
    url='https://roessner-network-solutions.com',
    license='GPLv2',
    author='Christian Roessner',
    author_email='christian@roessner.email',
    description='Complete management suite for DKIM keys in LDAP',
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=[
        'cryptography',
        'dnspython',
        'python-ldap',
        'colorama',
    ],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "Intended Audience :: Information Technology",
        "License :: OSI Approved :: GNU General Public License v2",
        "Natural Language :: English",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 3.9",
        "Topic :: System :: Systems Administration",
        "Topic :: Utilities"
    ],
    python_requires='>=3.9',
)
