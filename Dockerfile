FROM python:3.9-slim

MAINTAINER Christian Roessner <christian@roessner.email>

WORKDIR /usr/src/app

COPY etc ./etc
COPY requirements.txt ./

RUN set -ex ; apt-get -qq update || true ; \
       apt-get -qq --no-install-recommends install \
         gcc \
         libldap2-dev \
         libsasl2-dev

RUN pip install --no-cache-dir -r requirements.txt

RUN set -ex ; apt-get autoremove --purge -y gcc

COPY dkimmanager/dkimmanager.py ./

CMD [ "python", "./dkimmanager.py --auto --verbose --update-dns" ]

