import unittest

from dkimmanager.dkimmanager import DKIMKeys, DKIMKeyType


class TestDKIMKeys(unittest.TestCase):
    def setUp(self) -> None:
        self.dk = DKIMKeys()


class TestInit(TestDKIMKeys):
    def test_get_rsa_private_key(self) -> None:
        self.dk.generate_rsa()
        private_key = self.dk.get_rsa_private_key()
        self.assertIsNotNone(private_key, "Private RSA key is None")

    def test_get_rsq_public_key(self) -> None:
        self.dk.generate_rsa()
        public_key = self.dk.get_rsa_public_key()
        self.assertIsNotNone(public_key, "Public RSA key is None")

    def test_get_ed25519_private_key(self) -> None:
        self.dk.generate_ed25519()
        private_key = self.dk.get_ed25519_private_key()
        self.assertIsNotNone(private_key, "Private ED25519 is None")

    def test_get_ed25519_public_key(self) -> None:
        self.dk.generate_ed25519()
        public_key = self.dk.get_ed25519_public_key()
        self.assertIsNotNone(public_key, "Public ED25519 is None")

    def test_set_rsabits_1(self) -> None:
        self.dk.set_rsabits(4096)
        self.assertEqual(self.dk.rsabits, 4096)

    def test_set_rsabits_2(self) -> None:
        with self.assertRaises(ValueError) as ctx:
            self.dk.set_rsabits(768)
        self.assertTrue("RSA bits must be greater than 1024 bits",
                        ctx.exception)

    def test_get_private_key(self):
        self.dk.generate_rsa()
        self.dk.generate_ed25519()
        result = self.dk.get_private_key(DKIMKeyType.RSA)
        if result:
            self.assertIsInstance(result, str, "Result must be type str")
        result = self.dk.get_private_key(DKIMKeyType.ED25519)
        if result:
            self.assertIsInstance(result, str, "Result must be type str")

    def test_get_public_key(self):
        self.dk.generate_rsa()
        self.dk.generate_ed25519()
        result = self.dk.get_public_key(DKIMKeyType.RSA)
        if result:
            self.assertIsInstance(result, str, "Result must be type str")
        result = self.dk.get_public_key(DKIMKeyType.ED25519)
        if result:
            self.assertIsInstance(result, str, "Result must be type str")

    def test_generate_public_rsa(self):
        private_key = """
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEA1dlW6CteBlqMjma+DD6XC54RDGqeJK3sgtCYCRyQSQ1V8JxE
FweqJ+65yzFHtGh5rjQ15rdmpa6vO9cwgAYWMbWxa99lIYLMeyN1aNA2RzFX+/Yq
lYcX+CTLJRYpxn1/vv9isHCCzmIbuNL2uRjM1QezltOWUeQmuhv7G+vk1zplavKK
S8VX/se7bDmEmIceDZX7FLsflxk4lgiWRt88pTg9A7JjgvF7/MISl+Lkhxz05Tin
OVLLTqH88E8R3m4OomTInZCjAgJrDXPK5O1XLBzrGhx/lNovil7QOKSFVvVCTzs/
UEkfv56mBfHNPrlEvlC3Ob7l/+HWw8Go0wDZDQIDAQABAoIBAGaRFH7Mdg9kFLAk
FRj4WCfJS3ICHRGInbxsYc9U9Nsk20hJGttU4MbAWDpdy3968ImeHxAmfMQQkZmI
74kn4jM7avpLLk0xV/PjoOeBndbtkr6VZf1LyzJIK2sBQ04hWUj5YpMvk317TsKJ
5K4IeA3g4MEBgMKMRArVas4MiTi0uARlZ4HM53T/hHstDEy8NFSn+JZ31PD25Tks
BMVFLnLayLxwiA7hoQa/HItOhIK0bDdSNhu+Z+bZK9YeaBXmqfUvlePQ9LvnVW/B
Zz4l98TsldzN3EsyC/6+yBJUwAwAcnfmHquHu6ocO2nGtP8me5l9GauQo2SRHrer
59WO0MECgYEA7J6Borm7AaSWjeyA8JSszQbUWo9MVO/3CmtvoOnB2BTBHJ6fXFMn
WMtOgp9qt1esMrSrB6UfWerwBeV7FsxIj5CB6AqaJNRUjEsNUFmQoBDaOYFwAI1l
3bIne606xHHFn1vrRLc/03+8uh6PFdqfJGXOBGcuUf39BsKpBiM4hP0CgYEA511h
v7G8E5o04SYGAJKBHpkVKUTJukSnZS4+ghx9eTfN9NIqRqj4pn4c1uVcxbwJQueF
DVLlTRq++3AXpm7IJ9ZhJ3NhaChJfzfAwA0ybkGsoOOk+FaOqCbJPKw2MFX4+eux
VNCsyPsazA0obe43qvQ1K+xGGUui5fie/wPPaVECgYEA4H3Z/Gs+U2a2OFykUJyd
/b93BZxjDUrGFSqtM/vTn5DBTsfn5g42jHYHPMLG2r/hbhIfGhe9UPAnXVDFoXGb
finaNi6o5DxXIuup7AutvU9/24NEoiGE/fsinqJR2UMf/fuVkRJTCfRtOB/6HP/Y
cQ3tkhvaxIu23g0dk+cinnkCgYEA1UhZdvyis7Nhcfi6acakrw9R2aRnDoaBhh22
U/1tkdRm0Xz4qgDUj68MaPIxrrrkEsAZAKpdZdH1Ixc3Rh8z9r2C/Ho53mndLyC3
WJQR3GBAgmS4zJIGmm5+7qaWocJvbfzfkZ942SHalya+EXEb8kWDysTZ7jGyrqud
7tqVVrECgYAXR3c3CwDpVkxl4fjPiD2bvsHpmvFQ+MutPy5zIulsjZqM1h2vJiyf
IMGxGteMqK3r6hOozO/JmXOEIUV6pjhMNcO0X7IPdMRvMoeATBUClyf8qBV/tEnh
AIW3jDrBOHARrdu9o6kb0zV/wijLkTP8LmXYjrkB8hMyEH5rwkjjNg==
-----END RSA PRIVATE KEY-----
""".strip()
        self.dk.generate_public_rsa(private_key)
        result = self.dk.get_rsa_public_key()
        self.assertIsNotNone(result, "Result must not be None")

    def test_generate_public_ed25519(self):
        private_key = """
-----BEGIN PRIVATE KEY-----
MC4CAQAwBQYDK2VwBCIEIMkB4jirXXideRU2nNA08NE/52ZCocZnkHlZEUv1QtHz
-----END PRIVATE KEY-----
""".strip()
        self.dk.generate_public_ed25519(private_key)
        result = self.dk.get_ed25519_public_key()
        self.assertIsNotNone(result, "Result must not be None")
