import os
import unittest

from pprint import pprint

from dkimmanager import dkimmanager


class TestLDAPTree(unittest.TestCase):
    def setUp(self) -> None:
        print(os.getcwd())
        cf = dkimmanager.CfgFile("etc/opendkim-manage.cfg")
        self.lh = dkimmanager.LDAP(cf.ldap)
        self.ldaptree = dkimmanager.LDAPTree(cf.ldap)


class TestInit(TestLDAPTree):
    def test_parse_tree(self):
        dkimmanager.DEBUG = False
        self.ldaptree.parse_tree()

    def test_get_domain_by_domainname(self):
        dkimmanager.DEBUG = False
        self.assertIsNotNone(self.ldaptree.get_domain_by_domainname(
            "exampleserver.de"), "exampleserver.de must exist in LDAP!")

    def test_get_selectors_by_domainname(self):
        dkimmanager.DEBUG = False
        result = self.ldaptree.get_selectors_by_domainname("exampleserver.de")
        self.assertIsInstance(result, dict, "Selectorlist must be type list")

    def test_get_selector_by_domainname(self):
        dkimmanager.DEBUG = False
        result = self.ldaptree.get_selector_by_domainname(
            "exampleserver.de", "sFF912351CA-928D5A-0BEB32-2020-03")
        self.assertIsInstance(result, dkimmanager.LDAPTree.DKIMSelector,
                              "Result must be DKIMSelector")

    def test_get_state(self):
        dkimmanager.DEBUG = False
        result = self.ldaptree.get_state(
            "exampleserver.de", "sFF912351CA-928D5A-0BEB32-2020-03")
        self.assertIsInstance(result, dkimmanager.DKIMActiveState,
                              "Result must be DKIMActiveState")

    def test_get_key(self):
        dkimmanager.DEBUG = False
        result = self.ldaptree.get_key(
            "exampleserver.de", "sFF912351CA-928D5A-0BEB32-2020-03")
        self.assertRegex(
            result, "^revoked$", "Key not correct")

    def test_get_keytype_1(self):
        dkimmanager.DEBUG = False
        result = self.ldaptree.get_keytype(
            "exampleserver.de", "sFF912351CA-928D5A-0BEB32-2020-03")
        self.assertIsInstance(result, dkimmanager.DKIMKeyType,
                              "Result must be DKIMKeyType")

    def test_get_keytype_2(self):
        dkimmanager.DEBUG = False
        self.ldaptree.get_key(
            "exampleserver.de", "sFF912351CA-928D5A-0BEB32-2020-03")
        result = self.ldaptree.get_keytype(
            "exampleserver.de", "sFF912351CA-928D5A-0BEB32-2020-03")
        self.assertIsInstance(result, dkimmanager.DKIMKeyType,
                              "Result must be DKIMKeyType")

    def test_get_sorted_created_by_domainname(self):
        dkimmanager.DEBUG = False
        result = self.ldaptree.get_sorted_created_by_domainname(
            "exampleserver.de")
        if result:
            self.assertIsInstance(result, list, "Result must be type list")

    def test_get_sorted_modified_by_domainname(self):
        dkimmanager.DEBUG = False
        result = self.ldaptree.get_sorted_modified_by_domainname(
            "exampleserver.de")
        if result:
            self.assertIsInstance(result, list, "Result must be type list")

    def test_get_active_selector_by_domainname(self):
        dkimmanager.DEBUG = False
        result = self.ldaptree.get_active_selectors_by_domainname(
            "exampleserver.de")
        if result:
            self.assertIsInstance(result, list,
                                  "Result must be type DKIMSelector")

    def test_get_domainnames_with_empty_selectors(self):
        dkimmanager.DEBUG = False
        result = self.ldaptree.get_domainnames_with_empty_selectors()
        self.assertIsInstance(result, list, "Result must be type list")

    def test_has_key(self):
        dkimmanager.DEBUG = False
        result = self.ldaptree.has_dkimkey("exampleserver.de",
                                           "sFF912351CA-928D5A-0BEB32-2020-03")
        self.assertIsInstance(result, bool, "Result must be type bool")

    def test_get_domainnames(self):
        dkimmanager.DEBUG = False
        result = self.ldaptree.get_domainnames()
        self.assertIsInstance(result, list, "Result must be type list")

    def test_set_domainname(self):
        dkimmanager.DEBUG = False
        self.ldaptree.set_domainname("exampleserver.de")
        result = self.ldaptree.get_domainnames()
        self.assertIsInstance(result, list, "Result must be type list")

    def test_get_selector_1(self):
        dkimmanager.DEBUG = False
        result = self.ldaptree.get_selector("nonexist")
        self.assertIsNone(result)

    def test_get_selector_2(self):
        dkimmanager.DEBUG = False
        result = self.ldaptree.get_selector("sFF912351CA-928D5A-0BEB32-2020-03")
        self.assertIsInstance(result, dkimmanager.LDAPTree.DKIMSelector,
                              "Result type must be DKIMSelector")
