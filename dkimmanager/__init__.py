"""
opendkim-manage - DKIM managment tool for LDAP

Copyright (c) 2018-2020 by R.N.S.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
__author__ = "Christian Rößner"
__copyright__ = "Copyright (c) 2018 by R.N.S."
__credits__ = ["Andreas Schulze"]
__license__ = "GPL"
__version__ = "2020.01 (git: $Id$)"
__maintainer__ = "Christian Rößner"
__email__ = "christian@roessner.email"
__status__ = "Development"

__all__ = ['dkimmanager', 'DKIMKeys', 'DKIMKeyType']
