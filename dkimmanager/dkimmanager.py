#!/usr/bin/env python3
"""
opendkim-manage - DKIM managment tool for LDAP

Copyright (c) 2018-2020 by R.N.S.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import argparse
import configparser
import datetime
import hashlib
import inspect
import os
import random
import re
import string
import sys
import time
import traceback

from base64 import b64encode, b64decode

from _testcapi import traceback_print
from copy import copy
from enum import Enum, auto
from operator import itemgetter
from pprint import pformat
from typing import Optional, AnyStr, Match, Sequence, ClassVar, NoReturn, Any

import dns.query
import dns.resolver
import dns.tsigkeyring
import dns.update
import ldap
import ldap.modlist as modlist
import ldap.sasl
import ldapurl

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa, ed25519
from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PrivateKey
from cryptography.hazmat.primitives import serialization
from dns.tsig import HMAC_SHA256, HMAC_SHA384, HMAC_SHA512

try:
    from colorama import init, Fore, Style
    init()
    have_color = True
except ImportError:
    have_color = False


__author__ = "Christian Rößner"
__copyright__ = "Copyright (c) 2018 by R.N.S."
__credits__ = ["Andreas Schulze"]
__license__ = "GPL"
__version__ = "2020.01 (git: $Id$)"
__maintainer__ = "Christian Rößner"
__email__ = "christian@roessner.email"
__status__ = "Development"

NAME = "opendkim-manage"

DEBUG = True
VERBOSE = False
USE_COLOR = False
INTERACT = False


class DKIMKeyType(Enum):
    RSA = auto()
    ED25519 = auto()
    BOTH = auto()
    REVOKED = auto()
    Unknown = auto()


class DKIMActiveState(Enum):
    Enabled = auto()
    Disabled = auto()


class DKIMRevokeState(Enum):
    Enabled = auto()
    Disabled = auto()


class DKIMScheme(object):
    """Simple Mapping of attribute symbols and names
    """
    # Classes
    DKIM: ClassVar[str] = "DKIM"
    domain: ClassVar[str] = "domain"
    domainRelatedObject: ClassVar[str] = "domainRelatedObject"

    # Attributes
    DKIMKey: ClassVar[str] = "DKIMKey"
    DKIMKeyType: ClassVar[str] = "DKIMKeyType"
    DKIMIdentity: ClassVar[str] = "DKIMIdentity"
    DKIMSelector: ClassVar[str] = "DKIMSelector"
    DKIMActive: ClassVar[str] = "DKIMActive"
    DKIMDomain: ClassVar[str] = "DKIMDomain"
    associatedDomain: ClassVar[str] = "associatedDomain"
    destinationIndicator: ClassVar[str] = "destinationIndicator"
    createTimestamp: ClassVar[str] = "createTimestamp"
    modifyTimestamp: ClassVar[str] = "modifyTimestamp"


class GlobalCfg(object):
    """Global configuration options
    """
    delete_delay: ClassVar[int] = 10
    expire_after: ClassVar[int] = 365
    max_revoked: ClassVar[int] = 6
    keytype: ClassVar[DKIMKeyType] = DKIMKeyType.BOTH
    term_bg: ClassVar[str] = "dark"

    cname_selector_rsa_prefix: ClassVar[str] = "selector-rsa-"
    cname_selector_ed25519_prefix: ClassVar[str] = "selector-ed25519-"


class CfgFile(object):
    """Config file parser

    :param cfgfile: Parse a configuration file
    """
    def __init__(self, cfgfile: str):
        self.__config = configparser.ConfigParser()

        self.globals = dict()
        self.ldap = dict()
        self.dns = dict()

        # All known sections
        sections = (
            "global", "ldap", "dns"
        )

        # All known options
        global_opts = (
            "expire_after", "delete_delay", "selectorformat",
            "use_dkim_identity", "terminal_background", "keytype",
            "max_revoked", "cname_selector_rsa_prefix",
            "cname_selector_ed25519_prefix",
            "multiple_signatures_domains"
        )
        ldap_opts = (
            "uri", "bindmethod", "saslmech", "domain", "use_starttls",
            "reqcert", "ciphers", "cert", "key", "ca", "authz_id", "binddn",
            "bindpw", "destination_indicator"
        )
        dns_opts = (
            "primary_nameserver", "tsig_key_file", "tsig_key_name",
            "algorithm", "ttl", "cnames"
        )

        success = self.__config.read(cfgfile)
        if len(success) == 0:
            print(_c(f"Error: Config file '{cfgfile}' can not be read", "red"),
                  file=sys.stderr)
            sys.exit(os.EX_OSERR)

        for sec in iter(self.__config.sections()):
            if sec not in sections:
                print(_c(f"Error: Unknown section '{sec}'", "red)"),
                      file=sys.stderr)
                sys.exit(os.EX_SOFTWARE)

        if not self.__config.has_section("global"):
            print(_c("Error: Missing requried section 'global'", "red"),
                  file=sys.stderr)
            sys.exit(os.EX_SOFTWARE)
        if not self.__config.has_section("ldap"):
            print(_c("Error: Missing requried section 'ldap'", "red"),
                  file=sys.stderr)
            sys.exit(os.EX_SOFTWARE)

        # Parse global options
        for opt in iter(global_opts):
            self.globals[opt] = None
        s = "global"
        for opt in iter(self.__config.options(s)):
            if opt not in global_opts:
                print(_c(f"Error: Unknown option '{opt}' in section 'global'",
                         "red"), file=sys.stderr)
                sys.exit(os.EX_SOFTWARE)
            if opt in ("delete_delay", "expire_after", "max_revoked"):
                try:
                    self.globals[opt] = self.__config.getint(s, opt)
                except ValueError:
                    pass
            elif opt == "use_dkim_identity":
                try:
                    self.globals[opt] = self.__config.getboolean(s, opt)
                except ValueError:
                    pass
            elif opt == "keytype":
                if self.__config.get(s, opt) == "both":
                    self.globals[opt] = DKIMKeyType.BOTH
                elif self.__config.get(s, opt) == "rsa":
                    self.globals[opt] = DKIMKeyType.RSA
                elif self.__config.get(s, opt) == "ed25519":
                    self.globals[opt] = DKIMKeyType.ED25519
                else:
                    print(_c(f"Error: keytype must be one of 'both', 'rsa' or "
                             f"'ed25519'. Got: {self.__config.get(s, opt)}",
                             "red"), file=sys.stderr)
                    sys.exit(os.EX_SOFTWARE)
            elif opt == "multiple_signatures_domains":
                self.globals[opt] = [x.strip() for x in s.split(',')]
            else:
                self.globals[opt] = self.__config.get(s, opt)
        if self.globals['cname_selector_rsa_prefix'] is not None and \
                (self.globals['cname_selector_rsa_prefix'] ==
                 self.globals['cname_selector_ed25519_prefix']):
            print(_c(f"Error: CNAME prefix "
                     f"{self.globals['cname_selector_rsa_prefix']} "
                     f"MUST NOT be identical to CNAME prefix "
                     f"{self.globals['cname_selector_ed25519_prefix']}",
                     "red"), file=sys.stderr)
            sys.exit(os.EX_SOFTWARE)

        # Parse LDAP options
        for opt in iter(ldap_opts):
            self.ldap[opt] = None
        s = "ldap"
        for opt in iter(self.__config.options(s)):
            if opt not in ldap_opts:
                print(_c(f"Error: Unknown option '{opt}' in section 'ldap'",
                         "red"), file=sys.stderr)
                sys.exit(os.EX_SOFTWARE)
            if opt == "use_starttls":
                try:
                    self.ldap[opt] = self.__config.getboolean(s, opt)
                except ValueError:
                    self.ldap[opt] = False
            else:
                if opt == "domain":
                    DKIMScheme.associatedDomain = self.__config.get(s, opt)
                self.ldap[opt] = self.__config.get(s, opt)

        # Parse DNS options
        if "dns" in self.__config.sections():
            for opt in iter(dns_opts):
                self.dns[opt] = None
            s = "dns"
            if "algorithm" not in self.__config.options(s):
                self.dns['algorithm'] = HMAC_SHA256
            for opt in iter(self.__config.options(s)):
                if opt not in dns_opts:
                    print(_c(f"Error: Unknown option '{opt}' in section 'dns'",
                             "red"), file=sys.stderr)
                    sys.exit(os.EX_SOFTWARE)
                if opt == "algorithm":
                    value = self.__config.get(s, opt)
                    if value == "hmac_sha256":
                        self.dns[opt] = HMAC_SHA256
                    elif value == "hmac_sha384":
                        self.dns[opt] = HMAC_SHA384
                    elif value == "hmac_sha512":
                        self.dns[opt] = HMAC_SHA512
                    else:
                        print(_c(f"Error: {opt} unsupported HMAC: {value}",
                                 "red"), file=sys.stderr)
                        sys.exit(os.EX_SOFTWARE)
                    continue
                elif opt == "ttl":
                    try:
                        self.dns[opt] = self.__config.getint(s, opt)
                    except ValueError:
                        print(_c(f"Error: TTL in config file must be an "
                                 f"integer: {self.__config.get(s, opt)}",
                                 "red"), file=sys.stderr)
                        sys.exit(os.EX_SOFTWARE)
                    continue

                if opt == "tsig_key_file":
                    value = self.__config.get(s, opt)
                    if not os.path.exists(value):
                        print(_c(f"Error: Can not access TSIG key file "
                                 f"'{value}'", "red"), file=sys.stderr)
                        sys.exit(os.EX_SOFTWARE)

                self.dns[opt] = self.__config.get(s, opt)


class Cmd(object):
    """Command line parser
    """
    def __init__(self):
        global INTERACT, DEBUG, VERBOSE, USE_COLOR

        parser = argparse.ArgumentParser(prog=NAME)

        parser.add_argument("--list", "-l",
                            default=False,
                            action="store_true",
                            help="List DKIM keys")
        parser.add_argument("--create", "-c",
                            default=False,
                            action="store_true",
                            help="Create a new DKIM key")
        parser.add_argument("--delete", "-d",
                            default=False,
                            action="store_true",
                            help="Delete one or many DKIM keys")
        parser.add_argument("--force-delete",
                            default=False,
                            action="store_true",
                            help="Force deletion of a DKIM key")
        parser.add_argument("--active",
                            default=False,
                            action="store_true",
                            help="Set DKIMActive to TRUE for a selector")
        parser.add_argument("--force-active",
                            default=False,
                            action="store_true",
                            help="Force activation of a DKIM key")
        parser.add_argument("--age", "-A",
                            default=None,
                            type=int,
                            help="The key has to be more(+) or less (-) then n "
                                 "days old")
        parser.add_argument("--domain", "-D",
                            default=None,
                            action="append",
                            type=str,
                            help="A DNS domain name")
        parser.add_argument("--selectorname", "-s",
                            default=None,
                            action="append",
                            type=str,
                            help="A selector name")
        parser.add_argument("--size", "-S",
                            default=2048,
                            type=int,
                            help="Size of DKIM keys (default: %(default)s)")
        parser.add_argument("--keytype", "-k",
                            default=None,
                            type=str,
                            choices=["both", "rsa", "ed25519"]
                            )
        parser.add_argument("--testkey", "-t",
                            default=False,
                            action="store_true",
                            help="Check that the listed DKIM keys are "
                                 "published and useable")
        parser.add_argument("--config", "-f",
                            default="/etc/opendkim-manage.cfg",
                            type=str,
                            help=f"Path to '{NAME}' config file. " +
                                 "(default: '%(default)s'")
        parser.add_argument("--add-missing", "-m",
                            default=False,
                            action="store_true",
                            help="Add missing DKIM keys to LDAP objects")
        parser.add_argument("--max-initial",
                            default=0,
                            type=int,
                            help="Maximum number of newly created DKIM keys")
        parser.add_argument("--max-revoked", "-R",
                            default=GlobalCfg.max_revoked,
                            type=int,
                            help="Maximum number of revoked DKIM keys that "
                                 "shell be kept")
        parser.add_argument("--add-new", "-n",
                            default=False,
                            action="store_true",
                            help="Check age for DKIM keys and create new keys "
                                 "on demand")
        parser.add_argument("--rotate", "-r",
                            default=False,
                            action="store_true",
                            help="Rotate one or all DKIM keys")
        parser.add_argument("--auto", "-a",
                            default=False,
                            action="store_true",
                            help="Short for --add-missing, --add-new, --rotate "
                                 "and --delete")
        parser.add_argument("--print-dns",
                            default=False,
                            action="store_true",
                            help="Print out the public DNS information for a "
                                 "selector name")
        parser.add_argument("--accept-any-domain",
                            default=None,
                            action="store_true",
                            help="Do not report not existing domains as error")
        parser.add_argument("--expire-after", "-e",
                            default=None,
                            type=int,
                            help=f"Number of days after which new DKIM keys "
                                 f"will be created with --add-new (default: "
                                 f"{GlobalCfg.expire_after} days)")
        parser.add_argument("--delete-delay", "-y",
                            default=None,
                            type=int,
                            help=f"Delay deletion of old DKIM keys (default: "
                                 f"{GlobalCfg.delete_delay} days)")
        parser.add_argument("--update-dns", "-u",
                            default=False,
                            action="store_true",
                            help="Update DNS zones")

        parser.add_argument("--interactive", "-i",
                            default=False,
                            action="store_true",
                            help="Turn on interactive mode")

        parser.add_argument("--debug",
                            default=False,
                            action="store_true",
                            help="Turn on debugging")
        parser.add_argument("--verbose", "-v",
                            default=False,
                            action="store_true",
                            help="Verbose output")
        parser.add_argument("--color",
                            default=False,
                            action="store_true",
                            help="Turn on colors for output")
        parser.add_argument("--version", "-V",
                            default=False,
                            action="store_true",
                            help="Print version and exit")

        self.config = parser.parse_args()

        # Some options require the '--domain' option being set
        if self.config.create:
            if not self.config.domain:
                print(_c("Error: Option '--domain' is required", "red"),
                      file=sys.stderr)
                sys.exit(os.EX_USAGE)

        # Some options require the '--selectorname' option being set
        if (self.config.age or
                self.config.active or
                self.config.testkey) and not self.config.selectorname:
            print(_c("Error: Option '--selectorname' is required", "red"),
                  file=sys.stderr)
            sys.exit(os.EX_USAGE)

        # Make sure, we do not use these command on a per-domain or
        # per-selector basis
        if (self.config.add_missing or self.config.add_new or
            self.config.rotate or self.config.auto) and \
                (self.config.domain or self.config.selectorname):
            print(_c("Error: Option '--domain' and/or '--selectorname' not "
                     "allowed", "red"), file=sys.stderr)
            sys.exit(os.EX_USAGE)

        if self.config.testkey and \
                self.config.domain and \
                self.config.selectorname:
            print(_c("Error: Only one of '--domain' or '--selectorname' "
                     "is required", "red"), file=sys.stderr)
            sys.exit(os.EX_USAGE)

        # Either --selectorname and/or --domain is required
        if self.config.delete and not \
                (self.config.domain or self.config.selectorname):
            print(_c("Error: Option '--domain' and/or '--selectorname' is "
                     "required", "red"), file=sys.stderr)
            sys.exit(os.EX_USAGE)

        # Binary fields
        cmd_cleared = 0
        bit_mask = dict(m=1)

        # Bit shifter helper function
        def shift(x):
            x['m'] <<= 1
            return x['m']

        cmd_list = bit_mask['m']
        cmd_create = shift(bit_mask)
        cmd_delete = shift(bit_mask)
        cmd_rotate = shift(bit_mask)
        cmd_addmissing = shift(bit_mask)
        cmd_addnew = shift(bit_mask)
        cmd_print_dns = shift(bit_mask)
        cmd_auto = shift(bit_mask)

        # Only run one command at a time
        flags = cmd_cleared
        if self.config.list:
            flags |= cmd_list
        if self.config.create:
            flags |= cmd_create
        if self.config.delete:
            flags |= cmd_delete
        if self.config.rotate:
            flags |= cmd_rotate
        if self.config.add_missing:
            flags |= cmd_addmissing
        if self.config.add_new:
            flags |= cmd_addnew
        if self.config.print_dns:
            flags |= cmd_print_dns
        if self.config.auto:
            flags |= cmd_auto
        if flags != cmd_cleared:
            if flags != cmd_list and \
                    flags != cmd_create and \
                    flags != cmd_delete and \
                    flags != cmd_rotate and \
                    flags != cmd_addmissing and \
                    flags != cmd_addnew and \
                    flags != cmd_print_dns and \
                    flags != cmd_auto:
                print(_c("Error: Only one command at a time is allowed", "red"),
                      file=sys.stderr)
                sys.exit(os.EX_USAGE)

        DEBUG = self.config.debug
        VERBOSE = self.config.verbose
        USE_COLOR = self.config.color
        INTERACT = self.config.interactive


class LDAP(object):
    """LDAP connection class
    """
    def __init__(self, ldapcfg: dict):
        """
        :param ldapcfg: Dictionary that contains LDAP settings
        :var self.__ldapcfg: Configuration data structure for LDAP
        :var self.__uriparts: Contains split LDAP uri parts
        :var self.__starttls: Flag that indicates TLS usage
        :var self.__sasl: Flag that indicates SASL usage
        :var self.__auth_token: If SASL is defined, store an auth token
        :var self.__con: LDAP connection handle
        :var self.__custom_search_filter: The search filter for finding
        container objects for selectors can be overridden in the LDAP url
        :var self.configured: If the connection is initialized this flag is true
        :var self.__connected: If a LDAP connection exists this flag is true
        """
        self.__ldapcfg = ldapcfg
        self.__sasl = False
        self.__auth_token = None
        self.__con = None

        if not (self.__ldapcfg['uri'] and self.__ldapcfg['domain']):
            print(_c("Error: One of 'uri' or 'domain' is missing in "
                     "config file", "red"), file=sys.stderr)
            sys.exit(os.EX_SOFTWARE)
        if not ldapurl.isLDAPUrl(self.__ldapcfg['uri']):
            print(_c("Error: Invalid LDAP URI specified", "red"))
            sys.exit(os.EX_SOFTWARE)

        self.__urlparts = ldapurl.LDAPUrl(self.__ldapcfg['uri'])
        self.__custom_search_filter = self.__urlparts.filterstr

        self.configured = False
        self.connected = False

    def initialize(self) -> NoReturn:
        """Initialze an LDAP object
        """
        if self.__ldapcfg['reqcert']:
            if self.__ldapcfg['reqcert'] == "never":
                reqcert = ldap.OPT_X_TLS_NEVER
                dbg("reqcert=NEVER")
            elif self.__ldapcfg['reqcert'] == "allow":
                reqcert = ldap.OPT_X_TLS_ALLOW
                dbg("reqcert=ALLOW")
            elif self.__ldapcfg['reqcert'] == "try":
                reqcert = ldap.OPT_X_TLS_TRY
                dbg("reqcert=TRY")
            elif self.__ldapcfg['reqcert'] == "demand":
                reqcert = ldap.OPT_X_TLS_DEMAND
                dbg("reqcert=DEMAND")
            else:
                print(_c("Error: Unsupport 'reqcert' argument", "red"),
                      file=sys.stderr)
                sys.exit(os.EX_SOFTWARE)
            ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, reqcert)

        if self.__ldapcfg['ciphers']:
            ldap.set_option(
                ldap.OPT_X_TLS_CIPHER_SUITE, self.__ldapcfg['ciphers'])
            dbg("set ciphers")

        if self.__ldapcfg['cert']:
            if not os.path.exists(self.__ldapcfg['cert']):
                print(_c("Error: Can not read LDAP TLS cert file", "red"),
                      file=sys.stderr)
                sys.exit(os.EX_SOFTWARE)
            ldap.set_option(
                ldap.OPT_X_TLS_CERTFILE, self.__ldapcfg['cert'])
            dbg(f"Set cert to {self.__ldapcfg['cert']}")
        if self.__ldapcfg['key']:
            if not os.path.exists(self.__ldapcfg['key']):
                print(_c("Error: Can not read LDAP TLS key file", "red"),
                      file=sys.stderr)
                sys.exit(os.EX_SOFTWARE)
            ldap.set_option(
                ldap.OPT_X_TLS_KEYFILE, self.__ldapcfg['key'])
            dbg(f"Set key to {self.__ldapcfg['key']}")
        if self.__ldapcfg['ca']:
            if not os.path.exists(self.__ldapcfg['ca']):
                print(_c("Error: Can not read LDAP TLS CA file", "red"),
                      file=sys.stderr)
                sys.exit(os.EX_SOFTWARE)
            ldap.set_option(
                ldap.OPT_X_TLS_CACERTFILE, self.__ldapcfg['ca'])
            dbg(f"Set ca to {self.__ldapcfg['ca']}")

        if self.__ldapcfg['bindmethod'] == "sasl":
            if self.__ldapcfg['saslmech']:
                if self.__ldapcfg['binddn'] and self.__ldapcfg['bindpw']:
                    if self.__ldapcfg['saslmech'].lower() == "digest-md5":
                        self.__auth_token = ldap.sasl.digest_md5(
                            self.__ldapcfg['binddn'],
                            self.__ldapcfg['bindpw']
                        )
                        dbg("SASL/DIGEST-MD5")
                    elif self.__ldapcfg['saslmech'].lower() == "cram-md5":
                        self.__auth_token = ldap.sasl.cram_md5(
                            self.__ldapcfg['binddn'],
                            self.__ldapcfg['bindpw']
                        )
                        dbg("SASL/CRAM-MD5")
                else:
                    if self.__ldapcfg['saslmech'].lower() == "external":
                        self.__auth_token = ldap.sasl.external(
                            self.__ldapcfg['authz_id'])
                        dbg("SASL/EXTERNAL")
                    elif self.__ldapcfg['saslmech'].lower() == "gssapi":
                        self.__auth_token = ldap.sasl.gssapi(
                            self.__ldapcfg['authz_id'])
                        dbg("SASL/GSSAPI")

                dbg("SASL enabled")
                self.__sasl = True

        self.configured = True

    def connect(self) -> NoReturn:
        """Connect to a LDAP server
        """
        con_string = f"{self.__urlparts.urlscheme}://{self.__urlparts.hostport}"
        try:
            self.__con = ldap.initialize(con_string)
            dbg(f"connected to {con_string}")
            # Silently ignore starttls, if ldaps was given
            if self.__ldapcfg['use_starttls'] and \
                    self.__urlparts.urlscheme != "ldaps":
                self.__con.start_tls_s()
                dbg("STARTTLS")
            if self.__sasl:
                self.__con.sasl_interactive_bind_s("", self.__auth_token)
                dbg("SASL AUTH")
            else:
                if self.__ldapcfg['binddn'] and self.__ldapcfg['bindpw']:
                    self.__con.simple_bind_s(
                        self.__ldapcfg['binddn'], self.__ldapcfg['bindpw'])
                    dbg("Simple bind")
                else:
                    self.__con = None
                    return  # Treat this as being not connected!
        except Exception:
            print(_c("Error: Can not connect to LDAP server", "red"),
                  file=sys.stderr)
            sys.exit(os.EX_SOFTWARE)

        self.connected = True

    def search(self, search_filter: str = None, base: str = None,
               attrs: list = None, search_scope: int = ldap.SCOPE_SUBTREE) -> \
            Optional[list]:
        """Common LDAP search

        :param search_filter: Custom search filter for LDAP
        :param base: Custom LDAP search base with sub scope
        :param attrs: List of custom LDAP result attributes
        :param search_scope: specifies how broad the search context is
        :return: Return a result list
        """
        assert len(attrs) > 0

        if not self.configured:
            self.initialize()
        if self.configured:
            if not self.connected:
                self.connect()
            if self.connected and self.__con:
                if not base:
                    base = self.__urlparts.dn
                dbg(f"{base=}")
                dbg(f"{search_filter=}")
                dbg(f"{attrs=}")
                rid = self.__con.search(
                    base, search_scope, search_filter, attrs)
                raw_result = self.__con.result(rid, True, 60)
                dbg(f"raw_result={pformat(raw_result)}")
                if raw_result[0] is None:
                    self.__con.abandon(rid)
                    print(_c("WARN: LDAP search timed out", "cyan"))
                    return self._result()
                list_of_results = raw_result[1]
                return list_of_results
            else:
                return None

        return None

    def store_dkim_key(self, dn: str, pem_key: str, keytype: DKIMKeyType,
                       domain: str, signing_table_domain: str,
                       identity: str = None) -> NoReturn:
        """Store a new RSA PEM formated key in LDAP

        :param dn: The DN for the newly created LDAP object
        :param pem_key: PEM formated RSA key
        :param keytype: DKIM key type
        :param signing_table_domain: The domain field of a signing table in
        OpenDKIM
        :param domain: The domain name of the DKIM object
        :param identity: Optional string for DKIMidentity
        """
        attrs = dict()
        attrs['objectclass'] = [b"top",
                                bytes(f"{DKIMScheme.DKIM}", encoding='utf-8'),
                                bytes(
                                    f"{DKIMScheme.domainRelatedObject}",
                                    encoding='utf-8'
                                )]
        attrs[DKIMScheme.DKIMKey] = bytes(pem_key.strip(), encoding='utf-8')
        attrs[DKIMScheme.DKIMActive] = b"FALSE"
        attrs[DKIMScheme.associatedDomain] = bytes(domain, encoding='utf-8')
        attrs[DKIMScheme.DKIMDomain] = bytes(
            signing_table_domain, encoding='utf-8')
        attrs[DKIMScheme.DKIMKeyType] = bytes(
            "rsa" if keytype == DKIMKeyType.RSA else "ed25519",
            encoding='utf-8')
        if identity:
            attrs[DKIMScheme.DKIMIdentity] = bytes(identity, encoding='utf-8')

        ldif = modlist.addModlist(attrs)
        dbg(f"{ldif=}")

        if not self.configured:
            self.initialize()
        if self.configured:
            if not self.connected:
                self.connect()
            if self.connected and self.__con:
                self.__con.add_s(dn, ldif)

    def delete_dkim_key(self, dn: str) -> NoReturn:
        """Delete a LDAP object

        :param dn: The DN of the LDAP object that is to be deleted
        """
        if not self.configured:
            self.initialize()
        if self.configured:
            if not self.connected:
                self.connect()
            if self.connected and self.__con:
                self.__con.delete_s(dn)

    def unset_active(self, dn: str) -> NoReturn:
        """Set current DKIM object inactive

        :param dn: DN of the LDAP object for which the attribute is modified
        """
        if not self.configured:
            self.initialize()
        if self.configured:
            if not self.connected:
                self.connect()
            if self.connected and self.__con:
                try:
                    self.__con.modify_s(dn, [
                        (ldap.MOD_REPLACE, DKIMScheme.DKIMActive, b"FALSE")
                    ])
                except Exception as modify_err:
                    traceback.print_exc()
                    print(f"Could not modify entry for DN {dn}; "
                          f"error={modify_err}; continue anyways!")

    def set_active(self, dn: str) -> NoReturn:
        """Set current DKIM object active

        :param dn: DN of the LDPA object for which the attribute is modified
        """
        if not self.configured:
            self.initialize()
        if self.configured:
            if not self.connected:
                self.connect()
            if self.connected and self.__con:
                try:
                    self.__con.modify_s(dn, [
                        (ldap.MOD_REPLACE, DKIMScheme.DKIMActive, b"TRUE")
                    ])
                except Exception as modify_err:
                    traceback.print_exc()
                    print(f"Could not modify entry for DN {dn}; "
                          f"error={modify_err}; continue anyways!")

    def revoke_dkim_key(self, dn: str) -> NoReturn:
        """Set the DKIMKey value to "revoked"

        :param dn: DN of the LDPA object for which the attribute is modified
        """
        if not self.configured:
            self.initialize()
        if self.configured:
            if not self.connected:
                self.connect()
            if self.connected and self.__con:
                try:
                    self.__con.modify_s(dn, [
                        (ldap.MOD_REPLACE, DKIMScheme.DKIMKey, b"revoked")
                    ])
                except Exception as modify_err:
                    traceback.print_exc()
                    print(f"Could not modify entry for DN {dn}; "
                          f"error={modify_err}; continue anyways!")

    def rename_selectorname(self, dn: str, newselectorname: str) -> NoReturn:
        """Rename the DKIMSelector

        :param dn: DN of the LDPA object for which the attribute is modified
        :param newselectorname: Destination selector name
        """
        if not self.configured:
            self.initialize()
        if self.configured:
            if not self.connected:
                self.connect()
            if self.connected and self.__con:
                try:
                    self.__con.modrdn_s(
                        dn, DKIMScheme.DKIMSelector + '=' + newselectorname)
                except Exception as modify_err:
                    traceback.print_exc()
                    print(f"Could not modify entry for DN {dn}; "
                          f"error={modify_err}; continue anyways!")

    @property
    def custom_search_filter(self):
        return self.__custom_search_filter


class LDAPTree(object):
    """Scan a LDAP server and build internal data structer of all keys or
    scann a single object
    """
    class DKIMSelector(object):
        """Store all required information for a DKIM selector object from LDAP
        """
        def __init__(self):
            self.domainname = None
            self.selectorname = None
            self.ldapdn = None
            self.created = None
            self.modified = None
            self.state = DKIMActiveState.Disabled
            self.revokestate = DKIMRevokeState.Disabled
            self.keytype = None
            self.key = None

        def __repr__(self):
            return f"<DKIMSelector({self.ldapdn}, " \
                   f"domainname={self.domainname} " \
                   f"selectorname={self.selectorname} created=" \
                   f"{self.created}, modified={self.modified}, state=" \
                   f"{self.state}, revokestate={self.revokestate} " \
                   f"keytype={self.keytype}, key=hidden>"

    class Domain(object):
        """A Domain object corresponds to the LDAP object that holds a set of
        DKIM keys for a single domain"""
        def __init__(self):
            self.ldapdn = None
            self.domainname = None
            self.destination_indicator = None
            self.selectors = dict()

        def __repr__(self):
            return f"<Domain({self.ldapdn}, domainname={self.domainname} " \
                   f"destination_indicator={self.destination_indicator} " \
                   f"selectors={self.selectors})>"

    def __init__(self, ldapcfg: dict, domain: str = '*'):
        self.__ldapcfg = ldapcfg
        self.__domains = dict()
        self.__domain = domain
        self.__tree_is_loaded = False

    def parse_tree(self) -> NoReturn:
        """
        Load a subset of the LDAP tree containing domain and selecotr
        information. Build the central data structure which is queried by
        other methods of this class
        """
        lh = LDAP(self.__ldapcfg)

        # Get full list of domains
        attrs = [DKIMScheme.associatedDomain, DKIMScheme.destinationIndicator]
        if lh.custom_search_filter:
            search_filter = lh.custom_search_filter.format(self.__domain)
        else:
            search_filter = f"(&(objectClass={DKIMScheme.domain})(" \
                            f"{DKIMScheme.associatedDomain}={self.__domain}))"
        dbg(search_filter)
        tree = lh.search(search_filter=search_filter, attrs=attrs)

        for domain in tree:
            dmnobj = LDAPTree.Domain()
            domainname = domain[1][
                DKIMScheme.associatedDomain][0].decode('utf-8')
            dmnobj.domainname = domainname
            dmnobj.ldapdn = domain[0]
            if DKIMScheme.destinationIndicator in domain[1]:
                dmnobj.destination_indicator = \
                    domain[1][DKIMScheme.destinationIndicator][0].decode(
                        'utf-8')

            # Collect all available selectors for a domain
            attrs = [DKIMScheme.DKIMDomain, DKIMScheme.DKIMSelector,
                     DKIMScheme.DKIMActive, DKIMScheme.createTimestamp,
                     DKIMScheme.modifyTimestamp, DKIMScheme.DKIMKeyType]
            search_filter = f"(&(objectClass={DKIMScheme.DKIM})(" \
                            f"{DKIMScheme.DKIMDomain}={domainname}))"
            dbg(search_filter)
            selectors = lh.search(search_filter=search_filter, attrs=attrs)
            for selector in selectors:
                selobj = LDAPTree.DKIMSelector()
                selobj.ldapdn = selector[0]
                data = selector[1]
                if DKIMScheme.DKIMSelector not in data:
                    continue
                if DKIMScheme.DKIMKeyType in data:
                    keytype = data[DKIMScheme.DKIMKeyType][0].decode('utf-8')
                    if keytype.lower() == "rsa":
                        selobj.keytype = DKIMKeyType.RSA
                    if keytype.lower() == "ed25519":
                        selobj.keytype = DKIMKeyType.ED25519
                if DKIMScheme.DKIMDomain in data:
                    selobj.domainname = data[DKIMScheme.DKIMDomain][0]\
                        .decode('utf-8')
                if DKIMScheme.DKIMSelector in data:
                    selobj.selectorname = data[DKIMScheme.DKIMSelector][0]\
                        .decode('utf-8')
                if DKIMScheme.createTimestamp in data:
                    ct = data[DKIMScheme.createTimestamp][0].decode('utf-8')
                    selobj.created = Manager.convert_ldaptime_to_datetime(ct)
                if DKIMScheme.modifyTimestamp in data:
                    mt = data[DKIMScheme.modifyTimestamp][0].decode('utf-8')
                    selobj.modified = Manager.convert_ldaptime_to_datetime(mt)
                if not selobj.modified and selobj.created:
                    selobj.modified = selobj.created
                if DKIMScheme.DKIMActive in data:
                    state = data[DKIMScheme.DKIMActive][0].decode('utf-8')
                    if state == "TRUE":
                        selobj.state = DKIMActiveState.Enabled
                    elif state == "FALSE":
                        selobj.state = DKIMActiveState.Disabled
                dmnobj.selectors[data[DKIMScheme.DKIMSelector][0].decode(
                    'utf-8')] = selobj

            self.__domains[domainname] = dmnobj

        dbg(pformat(self.__domains))

        self.__tree_is_loaded = True

    def reload_selectors_by_domainname(self, domainname: str) -> NoReturn:
        """Reload the list of selectors for a domain name. This way we
        guarantee that changes have really been done on the LDAP server

        :param domainname: A domain name whose selectors must be refreshed
        """
        self.set_domainname(domainname)
        self.parse_tree()
        dbg(pformat(self.__domains))

    def get_domainnames(self) -> list[str]:
        """Return a list of all domain names that hold selectors
        """
        if not self.__tree_is_loaded:
            self.parse_tree()
        domainnames = list()
        for domainname in self.__domains.keys():
            domainnames.append(domainname)
        return domainnames

    def get_domain_by_domainname(self, domainname: str) -> Optional[Domain]:
        """Return a domain object lookuped up by a domain name

        :param domainname: A domain name that is searched
        :return: Domain object, return None, if it was not found
        """
        if not self.__tree_is_loaded:
            self.parse_tree()
        if domainname in self.__domains:
            return self.__domains[domainname]
        else:
            return None

    def get_selectors_by_domainname(self, domainname: str) -> Optional[dict]:
        """Return a list of selector objects looked up by its domain name

        :param domainname: A domain name
        :return: Dictionary with selectors for a domain. If not found return
        None
        """
        if not self.__tree_is_loaded:
            self.parse_tree()
        if domainname in self.__domains:
            return self.__domains[domainname].selectors
        return None

    def get_selector_by_domainname(self, domainname: str,
                                   selectorname: str) -> \
            Optional[DKIMSelector]:
        """Return a single DKIM selector object by its domain name

        :param domainname: A domain name
        :param selectorname: The name of the selector
        :return: Selector object. If not found a new empty object is returned
        """
        if not self.__tree_is_loaded:
            self.parse_tree()
        if domainname in self.__domains:
            if selectorname in self.__domains[domainname].selectors:
                return self.__domains[domainname].selectors[selectorname]
            else:
                return LDAPTree.DKIMSelector()
        return None

    def get_selector(self, selectorname: str) -> Optional[DKIMSelector]:
        """Search the entire structure for a given selector name

        :param selectorname: Name of a DKIM selector
        :return: A Selector object if found, else return None
        """
        domainnames = self.get_domainnames()
        for domainname in domainnames:
            if domainname in self.__domains:
                if selectorname in self.__domains[domainname].selectors:
                    return self.__domains[domainname].selectors[selectorname]
        return None

    def get_state(self, domainname: str, selectorname: str) -> \
            Optional[DKIMActiveState]:
        """Return the active state of a selector. Either it is enabled or
        disabled.

        :param domainname: A domain name
        :param selectorname: The DKIM selector name
        :return: Returns the state of the slector. If not found return None
        """
        result = self.get_selector_by_domainname(domainname, selectorname)
        if result is None:
            return None
        return result.state

    def get_revokestate(self, domainname: str, selectorname: str) -> \
            Optional[DKIMRevokeState]:
        """Return the revoke state of a selector. Either it is enabled or
        disabled.

        :param domainname: A domain name
        :param selectorname: The DKIM selector name
        :return: Returns the state of the slector. If not found return None
        """
        result = self.get_selector_by_domainname(domainname, selectorname)
        if result is None:
            return None
        if result.revokestate == DKIMRevokeState.Enabled:
            return result.revokestate
        self.get_key(domainname, selectorname)
        return result.revokestate

    def get_created(self, domainname: str, selectorname: str) -> \
            Optional[datetime.datetime]:
        """Return the createdTimestamp field from a DKIM selector converted
        to datetime.datetime

        :param domainname: A domain name
        :param selectorname: The name of a selector
        :return: A datetime.datetime object having the createTimestamp
        information. Return None if not found.
        """
        result = self.get_selector_by_domainname(domainname, selectorname)
        if result is None:
            return None
        return result.created

    def get_modified(self, domainname: str, selectorname: str) -> \
            Optional[datetime.datetime]:
        """Return the modifiedTimestamp field from a DKIM selector converted
        to datetime.datetime

        :param domainname: A domain name
        :param selectorname: The name of a selector
        :return: A datetime.datetime object having the modifiedTimestamp
        information. Return None if not found.
        """
        result = self.get_selector_by_domainname(domainname, selectorname)
        if result is None:
            return None
        return result.modified

    def get_key(self, domainname: str, selectorname: str) -> Optional[str]:
        """Query the LDAP server and return the DKIM key for a selector. If
        the key has been retrieved, store it internally for future use.

        :param domainname: A domain name
        :param selectorname: The selector name
        :return: RSA, ed25519 or "revoked" key for a selector. Return None if
        not found
        """
        selector = self.get_selector_by_domainname(domainname, selectorname)
        if selector is None:
            return None
        # If we already have the key, return it
        if selector.key:
            return selector.key
        base = selector.ldapdn
        lh = LDAP(self.__ldapcfg)
        attrs = [DKIMScheme.DKIMKey]
        search_filter = f"(objectClass={DKIMScheme.DKIM})"
        key = lh.search(search_filter=search_filter, base=base, attrs=attrs,
                        search_scope=ldap.SCOPE_BASE)
        # The following exception should not happen!
        try:
            result = key[0][1][DKIMScheme.DKIMKey][0].decode('utf-8')
        except IndexError:
            return None
        if result == "revoked":
            self.__domains[domainname].selectors[selectorname].revokestate = \
                DKIMRevokeState.Enabled
        self.__domains[domainname].selectors[selectorname].key = result
        return result

    def get_keytype(self, domainname: str, selectorname: str) -> \
            Optional[DKIMKeyType]:
        """Return the DKIM key algorithm for a selector

        :param domainname: A domain name
        :param selectorname: The selector name
        :return: A DKIM key algorithm if found, else return None
        """
        if not self.__tree_is_loaded:
            self.parse_tree()

        # If we already know the key type, we can return it
        if selectorname in self.__domains[domainname].selectors.keys():
            if self.__domains[domainname].selectors[selectorname].keytype:
                return self.__domains[domainname].selectors[
                    selectorname].keytype

        result = self.get_key(domainname, selectorname)
        if result is None:
            return None

        rsa_or_ed25519_marker = "-----BEGIN PRIVATE KEY-----"
        if result[:len(rsa_or_ed25519_marker)] == rsa_or_ed25519_marker:
            lines = result.split('\n')
            # NOTE: Hopefully this creteria is save enough
            if len(lines) <= 4:
                self.__domains[domainname].selectors[selectorname].keytype = \
                    DKIMKeyType.ED25519
                return DKIMKeyType.ED25519
            else:
                self.__domains[domainname].selectors[selectorname].keytype = \
                    DKIMKeyType.RSA
                return DKIMKeyType.RSA

        rsa_marker = "-----BEGIN RSA PRIVATE KEY-----"
        if result[:len(rsa_marker)] == rsa_marker:
            self.__domains[domainname].selectors[selectorname].keytype = \
                DKIMKeyType.RSA
            return DKIMKeyType.RSA

        return DKIMKeyType.Unknown

    def get_sorted_created_by_domainname(self, domainname: str) -> \
            Optional[list[tuple]]:
        """Return a sorted list of tuples that is sorted from newest to
        oldest DKIM key. Each tuple consists of two elements: The selector
        name and the createdTimestamp converted to datetime.datetime

        :param domainname: The domain name
        :return: List of tuples of selector names and there created date.
        Return None, if nothing was found
        """
        selectors = self.get_selectors_by_domainname(domainname)
        if selectors is None:
            return None
        unsorted_cts = list()
        for selectorname in selectors.keys():
            created = self.get_created(domainname, selectorname)
            unsorted_cts.append((selectorname, created))
        sorted_cts = sorted(unsorted_cts, key=lambda x: x[1], reverse=True)
        return sorted_cts

    def get_sorted_modified_by_domainname(self, domainname: str) -> \
            Optional[list[tuple]]:
        """Return a sorted list of tuples that is sorted from newest to
        oldest DKIM key. Each tuple consists of two elements: The selector
        name and the modifiedTimestamp converted to datetime.datetime

        :param domainname: The domain name
        :return: List of tuples of selector names and there modified date.
        Return None, if nothing was found
        """
        selectors = self.get_selectors_by_domainname(domainname)
        if selectors is None:
            return None
        unsorted_mts = list()
        for selectorname in selectors.keys():
            modified = self.get_modified(domainname, selectorname)
            unsorted_mts.append((selectorname, modified))
        sorted_mts = sorted(unsorted_mts, key=lambda x: x[1], reverse=True)
        return sorted_mts

    def get_active_selectors_by_domainname(self, domainname: str) -> \
            Optional[list[DKIMSelector]]:
        """Return a list of Selector obects that have an active state

        :param domainname: The domain name
        :return: List of Selector objects. Return None, if not found
        """
        selectors = self.get_selectors_by_domainname(domainname)
        active_selectors = list()
        if selectors is None:
            return None
        for selectorname in selectors.keys():
            state = self.get_state(domainname, selectorname)
            if state is None:
                continue
            if state == DKIMActiveState.Enabled:
                active_selectors.append(self.get_selector_by_domainname(
                    domainname, selectorname))
        return active_selectors

    def get_domainnames_with_empty_selectors(self) -> list[str]:
        """Scan the LDAP strcuture and return a list of domain names that do
        not yet have any selectors in it.

        :return: List of domain names without selectors
        """
        if not self.__tree_is_loaded:
            self.parse_tree()
        empty_domains = list()
        for domainname in self.__domains:
            selectors = self.get_selectors_by_domainname(domainname)
            if not selectors:
                empty_domains.append(domainname)
        return empty_domains

    def has_dkimkey(self, domainname: str, selectorname: str) -> bool:
        """Return boolean that indicates if a selector has an active or
        passive DKIM key

        :param domainname: A domain name
        :param selectorname: The selector name
        :return: Boolean
        """
        key = self.get_keytype(domainname, selectorname)
        if key is None:
            return False
        if key in (DKIMKeyType.REVOKED, DKIMKeyType.Unknown):
            return False
        if key in (DKIMKeyType.RSA, DKIMKeyType.ED25519):
            return True
        return False

    def set_domainname(self, domainname: str) -> NoReturn:
        """Set the domain name of a Domain object

        :param domainname: The domain name
        """
        self.__domain = domainname


class NoSelectornameError(Exception):
    """Exception that is raised, if a selector name does not exist
    """
    pass


class SelectorParseError(IndexError):
    """Exception that is raised, if the selector format can not be parsed
    """
    pass


class Selector(object):
    """Generate a selector from config file param 'selectorname'

    :param __pattern: Private class pattern that matches variables in the
    'selectorformat' string
    :param __prog: Regular expression compiled object
    :param name: Property fget function that returns the computed name for
    the 'selectorformat' string
    """
    def __init__(self, cfgarg: AnyStr):
        """
        :param cfgarg: Value for the 'selectorformat' string from config file
        :var self.__cfgarg: Stores the selector format string
        :var self.__name: Computed selector name
        """
        self.__cfgarg = cfgarg
        self.__name = None

    def get_name(self) -> str:
        """Property helper method

        :return: The computed selector name from the format string
        """
        if self.__name is None:
            raise NoSelectornameError()
        return self.__name

    def parse(self) -> NoReturn:
        """Parse the selector format and store the result in the 'name'
        attribute
        """
        self.__name = self.__prog.sub(self.__repl, self.__cfgarg)

    @staticmethod
    def __repl(matchobj: Match[AnyStr]) -> AnyStr:
        """Replaces format string pattern defined in 'selectorformat'

        :param matchobj: regular expression match object
        :return: Replacement for found groups
        """
        if matchobj.group(1):
            dbg(f"group(1): {matchobj.group(1)}")
            key = matchobj.group(1)
            length = 0

            if matchobj.group(2):
                dbg(f"group(2): {matchobj.group(2)}")
                length = int(matchobj.group(2)[1:])

            repl = ""
            if key.lower() == "randomhex":
                lst = [random.choice(
                    string.ascii_letters + string.digits) for _ in range(128)]
                rand = bytes("".join(lst), encoding='utf-8')

                if length == 0:
                    raise SelectorParseError()

                repl = hashlib.sha256(rand).hexdigest()[:length].upper()

            elif key.lower() == "year":
                repl = str(datetime.datetime.now().year)
            elif key.lower() == "month":
                repl = f"{datetime.datetime.now().month:02d}"
            elif key.lower() == "day":
                repl = f"{datetime.datetime.now().day:02d}"

            return repl
        else:
            return matchobj.group(0)

    # We allow random hex between 1 and 20 chars
    __pattern = r"\$\{(\w+)(:[0-9]+)?\}"
    __prog = re.compile(__pattern, flags=re.UNICODE | re.IGNORECASE)

    name = property(fget=get_name)


class InvalidSelectorname(Exception):
    """Exception that is raised, if the created selectorname is invalid
    """
    pass


class InvalidTSIGKeyFile(Exception):
    """Raise an exception, if the content of the TSIG key file is wrong
    """
    pass


class DNS(object):
    """Add and delete DNS records
    """
    def __init__(self, cf: CfgFile):
        """
        :param cf: config file object
        """
        self.__cf = cf
        if cf.dns['tsig_key_name']:
            self.__keyring = dns.tsigkeyring.from_text({
                self.__cf.dns['tsig_key_name']: self._get_key_from_file()
            })
        else:
            self.__keyring = None
        dbg(f"{self.__keyring=}")

    def add_dkim_key(self, zone: str, selectorname: str, rcontent: str,
                     subdomain: str = None) -> int:
        """Add a TXT record for a DKIM key

        :param zone: The domainname of the zone
        :param selectorname: The selectorname name
        :param rcontent: Text for the TXT record. The string must not be
        :param subdomain: An optional subdomain
        normalized for DNS, as this module does take care for it.
        """
        update = dns.update.Update(zone,
                                   keyring=self.__keyring,
                                   keyalgorithm=self.__cf.dns['algorithm'])
        if subdomain:
            rname = selectorname + "._domainkey." + subdomain
        else:
            rname = selectorname + "._domainkey"
        dbg(rname)
        rcontent = DNS.make254(rcontent)
        dbg(rcontent)
        update.add(rname, self.__cf.dns['ttl'], 'TXT', rcontent)
        response = dns.query.tcp(update, self.__cf.dns['primary_nameserver'])
        dbg(response)
        if response.rcode() == dns.rcode.NOERROR:
            verbose(_c(f"'{rname}.{zone}.'", "yellow") + " TXT: '" +
                    _c(f"{rcontent}", "blue") + "' created")
        else:
            verbose(_c(f"'{rname}.{zone}.'", "yellow") + " TXT: '" +
                    _c(f"{rcontent}", "red") + "' failed")
        return response.rcode()

    def remove_dkim_key(self, zone: str, selectorname: str,
                        subdomain: str = None) -> int:
        """Remove a DKIM TXT record from DNS

        :param zone: The domainname of the zone
        :param selectorname: The selectorname name
        :param subdomain: An optional subdomain
        """
        update = dns.update.Update(zone,
                                   keyring=self.__keyring,
                                   keyalgorithm=self.__cf.dns['algorithm'])
        if subdomain:
            rname = selectorname + "._domainkey." + subdomain
        else:
            rname = selectorname + "._domainkey"
        dbg(rname)
        update.delete(rname)
        response = dns.query.tcp(update, self.__cf.dns['primary_nameserver'])
        dbg(response)
        if response.rcode() == dns.rcode.NOERROR:
            verbose(_c(f"'{rname}.{zone}.'", "yellow") + " deleted")
        else:
            verbose(_c(f"'{rname}.{zone}.'", "yellow") + " TXT: '" +
                    _c(f"{rcontent}", "red") + "' failed")
        return response.rcode()

    def change_dkim_key(self, zone: str, selectorname: str,
                        rcontent: str, subdomain: str = None) -> int:
        """Change a TXT record for a DKIM key and set the p-field to p=

        :param zone: The domainname of the zone
        :param selectorname: The selectorname name
        :param rcontent: Text for the TXT record.
        :param subdomain: An optional subdomain
        """
        update = dns.update.Update(zone,
                                   keyring=self.__keyring,
                                   keyalgorithm=self.__cf.dns['algorithm'])
        if subdomain:
            rname = selectorname + "._domainkey." + subdomain
        else:
            rname = selectorname + "._domainkey"
        dbg(rname)
        rcontent = DNS.make254(rcontent)
        dbg(rcontent)
        update.replace(rname, self.__cf.dns['ttl'], 'TXT', rcontent)
        response = dns.query.tcp(update, self.__cf.dns['primary_nameserver'])
        dbg(response)
        dbg(response)
        if response.rcode() == dns.rcode.NOERROR:
            verbose(_c(f"'{rname}.{zone}.'", "yellow") + " TXT: '" +
                    _c(f"{rcontent}", "blue") + "' updated")
        else:
            verbose(_c(f"'{rname}.{zone}.'", "yellow") + " TXT: '" +
                    _c(f"{rcontent}", "red") + "' failed")
        return response.rcode()

    def _get_key_from_file(self) -> str:
        """Read a TSIG formatted key and extract the pure key
        """
        with open(self.__cf.dns['tsig_key_file']) as f:
            while True:
                line = f.readline()
                if line == "":
                    break
                if line.startswith("Key:"):
                    tsig_key = line.split("Key:")[1].strip()
                    dbg(f"{tsig_key=}")
                    return tsig_key
        raise InvalidTSIGKeyFile("TSIG key file format error")

    @staticmethod
    def make254(arg: str) -> str:
        """A string must not be more than 255 bytes. Split the record in chunks

        :param arg: A TXT string that must be split into chunks
        :return: Normalized string suiteable for DNS updates
        """
        return " ".join(
            ['"' + arg[i:i+254] + '"' for i in range(0, len(arg), 254)])

    @staticmethod
    def calculated_rname(selectorname: str, domainname: str) -> str:
        """Check, if the given record name conforms to DNS RFC1035
        """
        # The constructed rname
        record = selectorname + "._domainkey." + domainname

        labels = record.split('.')
        count = 0
        # Count labels
        for label in iter(labels):
            if len(label) > 63:
                raise InvalidSelectorname(
                    f"Label '{label}' too long: {len(label)} > 63")
            count += len(label)

        # Count dots
        count += len(labels) - 1

        if count > 253:
            raise InvalidSelectorname(f"Record too long: {count} > 253")

        return selectorname


class DKIMKeys(object):
    """Generate RSA and ed25519 keys
    """
    def __init__(self):
        self.__rsa_private_key = None
        self.__rsa_public_key = None
        self.__rsabits = 2048
        self.__ed25519_private_key = None
        self.__ed25519_public_key = None

    def generate_rsa(self) -> NoReturn:
        """Generate a RSA key pair
        """
        private_key = rsa.generate_private_key(
            public_exponent=65537, key_size=self.__rsabits,
            backend=default_backend()
        )
        # noinspection PyTypeChecker
        _pem_private_key = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption()
        )
        # noinspection PyTypeChecker
        _der_public_key = private_key.public_key(
        ).public_bytes(
            encoding=serialization.Encoding.DER,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        self.__rsa_private_key = str(_pem_private_key, encoding='utf-8')
        dbg(f"{self.__rsa_private_key=}")
        self.__rsa_public_key = b64encode(_der_public_key).decode('utf-8')
        dbg(f"{self.__rsa_public_key=}")

    # noinspection PyTypeChecker
    def generate_public_rsa(self, private_key: str) -> NoReturn:
        """Generate a public RSA key from a private key

        :param private_key: Private RSA key
        """
        loaded_private_key = serialization.load_pem_private_key(
            bytes(private_key, encoding='utf-8'),
            backend=default_backend(),
            password=None
        )
        der_public_key = loaded_private_key.public_key().public_bytes(
            encoding=serialization.Encoding.DER,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        self.__rsa_public_key = b64encode(der_public_key).decode('utf-8')
        dbg(f"{self.__rsa_public_key=}")

    def generate_ed25519(self) -> NoReturn:
        """Generate an ed25519 key pair
        """
        private_key = Ed25519PrivateKey.generate()

        # noinspection PyTypeChecker
        _pem_private_key = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        )

        # noinspection PyTypeChecker
        _raw_public_key = private_key.public_key(
        ).public_bytes(
            encoding=serialization.Encoding.Raw,
            format=serialization.PublicFormat.Raw
        )
        self.__ed25519_private_key = str(_pem_private_key, 'utf-8')
        dbg(f"{self.__ed25519_private_key=}")
        self.__ed25519_public_key = b64encode(_raw_public_key).decode('utf-8')
        dbg(f"{self.__ed25519_public_key=}")

    # noinspection PyTypeChecker
    def generate_public_ed25519(self, private_key: str) -> NoReturn:
        """Generate an ed25519 public key from a private key

        :param private_key: A private ed25519 key in PEM format
        """
        loaded_private_key = serialization.load_pem_private_key(
            bytes(private_key, encoding='utf-8'),
            backend=default_backend(),
            password=None,
        )
        public_key = loaded_private_key.public_key().public_bytes(
            encoding=serialization.Encoding.Raw,
            format=serialization.PublicFormat.Raw
        )
        self.__ed25519_public_key = b64encode(public_key).decode('utf-8')
        dbg(f"{self.__ed25519_public_key=}")

    def get_rsa_private_key(self) -> Optional[str]:
        """Return the private RSA key

        :return: Private RSA key
        """
        return self.__rsa_private_key

    def get_rsa_public_key(self) -> Optional[str]:
        """Return the public RSA key

        :return: Public RSA key
        """
        return self.__rsa_public_key

    def get_ed25519_private_key(self) -> Optional[str]:
        """Return the private ed25519 key

        :return: Private ed25519 key
        """
        return self.__ed25519_private_key

    def get_ed25519_public_key(self) -> Optional[str]:
        """Return the public ed25519 key

        :return: Public ed25519 key
        """
        return self.__ed25519_public_key

    def set_rsabits(self, value: int) -> NoReturn:
        """Set the number of bits for the RSA algorithm. Allowed values start
        at 1024

        :param value: Integer value
        """
        if value > 1024:
            self.__rsabits = value
        else:
            raise ValueError("RSA bits must be greater than 1024 bits")

    def get_private_key(self, keytype: DKIMKeyType) -> Optional[str]:
        """Return a private key specified by key algorithm

        :param keytype: Either a RSA or ed25519 key type
        :return: Private key
        """
        if keytype == DKIMKeyType.RSA:
            return self.get_rsa_private_key()
        if keytype == DKIMKeyType.ED25519:
            return self.get_ed25519_private_key()
        return None

    def get_public_key(self, keytype: DKIMKeyType) -> Optional[str]:
        """Return a public key specified by key algorithm

        :param keytype: Either a RSA or ed25519 key type
        :return: Public key
        """
        if keytype == DKIMKeyType.RSA:
            return self.get_rsa_public_key()
        if keytype == DKIMKeyType.ED25519:
            return self.get_ed25519_public_key()
        return None

    @property
    def rsabits(self) -> int:
        return self.__rsabits

    @rsabits.setter
    def rsabits(self, value: int) -> NoReturn:
        self.set_rsabits(value)


class Manager(object):
    """Main class that handles all features of opendkim-manage
    """
    def __init__(self, cf: CfgFile, cmd: Cmd):
        """
        :param cf: Config file object from a Cfg() call
        :param cmd: Command line object from a Cmd() call
        :var self.__cf: Stores config file data
        :var self.__cmd: Stores the command line parameters
        :var self.__ldaptree Holds an LDAPTree object
        """
        self.__cf = cf
        self.__cmd = cmd
        self.__ldap = LDAP(cf.ldap)
        self.__ldaptree = LDAPTree(cf.ldap)

    def cmd_list(self) -> NoReturn:
        """--list command

        Scan one or all domains and display the DKIMSelector attributes. If
        the object contains a DKIMActive attribute set to TRUE, mark the
        output as active.
        """
        dbg("Entering")

        if self.__cmd.config.domain and len(self.__cmd.config.domain) > 0:
            domains = self.__cmd.config.domain
        else:
            domains = self.__ldaptree.get_domainnames()
        dbg(domains)

        for domainname in iter(domains):
            domain = self.__ldaptree.get_domain_by_domainname(domainname)
            dbg(domain)
            if not domain:
                continue
            if not domain.ldapdn:
                continue

            print("DNS domain '" + _c(f"{domainname}", "blue") + "':")
            print("DN: " + _c(f"{domain.ldapdn}", "yellow"))

            for selectorname in domain.selectors.keys():
                keystate = self.__ldaptree.get_state(domainname, selectorname)
                revokestate = self.__ldaptree.get_revokestate(
                    domainname, selectorname)
                dbg(keystate)
                if not keystate:
                    continue

                if keystate == DKIMActiveState.Enabled:
                    state = " (" + _c("active", "red") + ")"
                else:
                    state = ""

                if revokestate == DKIMRevokeState.Enabled:
                    revstate = " [" + _c("revoked", "blue") + "]"
                else:
                    revstate = ""

                keytype = self.__ldaptree.get_keytype(domainname, selectorname)
                dbg(keytype)
                if not keytype:
                    continue

                created = self.__ldaptree.get_created(domainname, selectorname)
                dbg(created)
                if not created:
                    continue

                print(f"{utc2local(created)} DKIMSelector: " +
                      _c(f"{selectorname}", "magenta") + " " +
                      _c(str(keytype), "yellow") +
                      f"{state}{revstate}")

        dbg("Leaving")

    # noinspection PyPropertyAccess
    def cmd_create(self, domainname: str = None,
                   explicit_keytype: DKIMKeyType = None) -> NoReturn:
        """--create command

        Either create a new LDAP object for DKIM manually, if a custom
        selector was specified or use the 'selectorformat' string to compute
        one on the fly.
        """
        dbg("Entering")

        if domainname:
            domains = [domainname]
        else:
            domains = self.__cmd.config.domain

        # Make sure we only use one selector and one domain in manual mode!
        if self.__cmd.config.selectorname:
            if len(domains) > 1:
                print(_c("Error: Too many domains specified", "red"),
                      file=sys.stderr)
                sys.exit(os.EX_SOFTWARE)
            if len(self.__cmd.config.selectorname) > 1:
                print(_c("Error: Please specify only one 'selectorname'",
                         "red"), file=sys.stderr)
                sys.exit(os.EX_SOFTWARE)
            if GlobalCfg.keytype == DKIMKeyType.BOTH:
                print(_c("Error: Please specify only one 'keytype'",
                         "red"), file=sys.stderr)
                sys.exit(os.EX_SOFTWARE)

        del domainname

        for domainname in domains:
            need_reload = False

            # Get DN suffix for the new selectorname object
            domain = self.__ldaptree.get_domain_by_domainname(domainname)
            dbg(domain)
            if not domain:
                continue

            # Empty selector will never happen
            new_selectorname = ""

            keytypes = list()
            dkim_keys = DKIMKeys()

            # Enforce special key types...
            if explicit_keytype == DKIMKeyType.BOTH:
                keytypes.extend([DKIMKeyType.RSA, DKIMKeyType.ED25519])
            elif explicit_keytype == DKIMKeyType.RSA:
                keytypes.append(DKIMKeyType.RSA)
            elif explicit_keytype == DKIMKeyType.ED25519:
                keytypes.append(DKIMKeyType.ED25519)

            # ...or use key type from global config
            elif GlobalCfg.keytype == DKIMKeyType.BOTH:
                if self.__cmd.config.selectorname:
                    print("Error: Only one key type allowed!", file=sys.stderr)
                    sys.exit(os.EX_SOFTWARE)
                keytypes.extend([DKIMKeyType.RSA, DKIMKeyType.ED25519])
            elif GlobalCfg.keytype == DKIMKeyType.RSA:
                keytypes.append(DKIMKeyType.RSA)
            elif GlobalCfg.keytype == DKIMKeyType.ED25519:
                keytypes.append(DKIMKeyType.ED25519)

            for keytype in keytypes:
                # Use command line selector
                if self.__cmd.config.selectorname:
                    try:
                        new_selectorname = DNS.calculated_rname(
                            self.__cmd.config.selectorname[0], domainname)
                    except InvalidSelectorname as rname_err:
                        print(_c(f"Error: DNS rname error for selector "
                                 f"'{self.__cmd.config.selectorname[0]}': "
                                 f"{rname_err}", "red"), file=sys.stderr)
                        sys.exit(os.EX_SOFTWARE)

                # Create selector from selectorformat
                if self.__cf.globals['selectorformat'] and not \
                        self.__cmd.config.selectorname:
                    sel = Selector(self.__cf.globals['selectorformat'])
                    try:
                        sel.parse()
                    except SelectorParseError:
                        print(_c("Error: 'selectorformat' is invalid", "red"),
                              file=sys.stderr)
                        sys.exit(os.EX_SOFTWARE)

                    try:
                        new_selectorname = DNS.calculated_rname(
                            sel.name, domainname)
                    except InvalidSelectorname as rname_err:
                        print(_c(f"Error: DNS rname error for selector "
                                 f"'{sel.name}': {rname_err}", "red"),
                              file=sys.stderr)
                        sys.exit(os.EX_SOFTWARE)

                    # We can  only created both types of keys, if we use a
                    # selectorformat
                    if explicit_keytype == DKIMKeyType.BOTH:
                        if keytype == DKIMKeyType.RSA:
                            dkim_keys.set_rsabits(self.__cmd.config.size)
                            dkim_keys.generate_rsa()
                        if keytype == DKIMKeyType.ED25519:
                            dkim_keys.generate_ed25519()
                    elif GlobalCfg.keytype == DKIMKeyType.BOTH:
                        if keytype == DKIMKeyType.RSA:
                            dkim_keys.set_rsabits(self.__cmd.config.size)
                            dkim_keys.generate_rsa()
                        if keytype == DKIMKeyType.ED25519:
                            dkim_keys.generate_ed25519()

                if explicit_keytype == keytype == DKIMKeyType.RSA:
                    dkim_keys.set_rsabits(self.__cmd.config.size)
                    dkim_keys.generate_rsa()
                elif GlobalCfg.keytype == keytype == DKIMKeyType.RSA:
                    dkim_keys.set_rsabits(self.__cmd.config.size)
                    dkim_keys.generate_rsa()
                if explicit_keytype == keytype == DKIMKeyType.ED25519:
                    dkim_keys.generate_ed25519()
                elif GlobalCfg.keytype == keytype == DKIMKeyType.ED25519:
                    dkim_keys.generate_ed25519()

                # Neither command line selector nor computed selector
                if not self.__cf.globals['selectorformat'] and not \
                        self.__cmd.config.selectorname:
                    print(_c("WARN: No 'selectorformat' defined", "cyan"),
                          file=sys.stderr)
                    break

                # Check, if selector already exists
                for selectorname in domain.selectors.keys():
                    if selectorname == new_selectorname:
                        print(_c(f"Error: Object for selectorname "
                                 f"'{new_selectorname}' already exists", "red"),
                              file=sys.stderr)
                        sys.exit(os.EX_SOFTWARE)

                private_key = dkim_keys.get_private_key(keytype)
                public_key = dkim_keys.get_public_key(keytype)

                if INTERACT:
                    print(private_key)

                if self.ask("Do you want to save the DKIM key in LDAP?"):
                    if self.__cf.globals['use_dkim_identity']:
                        identity = "@" + domainname
                    else:
                        identity = None

                    dbg("Saving DKIM key to LDAP")
                    signing_table_domain = domainname
                    if "multiple_signatures_domains" in self.__cf.globals:
                        if domainname in self.__cf.globals[
                                "multiple_signatures_domains"]:
                            signing_table_domain = '*'
                    self.__ldap.store_dkim_key(
                        f"{DKIMScheme.DKIMSelector}={new_selectorname}," +
                        domain.ldapdn, private_key, keytype, domainname,
                        signing_table_domain, identity)
                    verbose("DN: " +
                            _c(f"{DKIMScheme.DKIMSelector}={new_selectorname}" +
                               domain.ldapdn, "yellow") +
                            _c(f" {keytype=}", "blue") + " created")
                    need_reload = True

                    if domain.destination_indicator:
                        continue
                    if self.__cmd.config.update_dns:
                        ns = DNS(self.__cf)
                        alg = ""
                        if keytype == DKIMKeyType.RSA:
                            alg = "rsa"
                        if keytype == DKIMKeyType.ED25519:
                            alg = "ed25519"
                        rcontent = f"v=DKIM1; k={alg}; h=sha256; p={public_key}"
                        ns.add_dkim_key(domainname, new_selectorname, rcontent)

            if need_reload:
                dbg(f"Reloading data for {domainname=}")
                self.__ldaptree.reload_selectors_by_domainname(domainname)

        dbg("Leaving")

    def cmd_delete(self, domainname: str = None, selectorname: str = None,
                   force_delete: bool = False) -> NoReturn:
        """--delete command

        Delete one or more DKIM objects from LDAP

        :param domainname: Optional domainname for the given selector
        :param selectorname: Optional selectorname to delete
        :param force_delete: Enforcing delete
        """
        dbg("Entering")

        def dns_delete() -> bool:
            """Helper that gets its parameters while runtime.

            :return: boolean, if DNS operations were successfull
            """
            domain = self.__ldaptree.get_domain_by_domainname(domainname)
            if self.__cmd.config.update_dns:
                zone = domainname
                subdomain = None
                if domain.destination_indicator:
                    subdomain = domainname.replace('.', '_')
                    zone = domain.destination_indicator
                try:
                    ns = DNS(self.__cf)
                    ns.remove_dkim_key(zone, selectorname, subdomain=subdomain)
                except Exception as ns_err:
                    print(_c(f"WARN: DNS error: {ns_err}", "cyan"),
                          file=sys.stderr)
                    return False
            return True

        def dns_revoke() -> bool:
            """Helper that gets its parameters while runtime.

            :return: boolean, if DNS operations were successfull
            """
            domain = self.__ldaptree.get_domain_by_domainname(domainname)
            if self.__cmd.config.update_dns:
                zone = domainname
                subdomain = None
                if domain.destination_indicator:
                    subdomain = domainname.replace('.', '_')
                    zone = domain.destination_indicator
                try:
                    ns = DNS(self.__cf)
                    ns.change_dkim_key(zone, selectorname, content,
                                       subdomain=subdomain)
                except Exception as ns_err:
                    print(_c(f"WARN: DNS error: {ns_err}", "cyan"),
                          file=sys.stderr)
                    return False
            return True

        if selectorname:
            selectors = [selectorname]
        else:
            if domainname:
                selectors = self.__ldaptree.get_selectors_by_domainname(
                    domainname)
            else:
                selectors = self.__cmd.config.selectorname

        del selectorname

        domainnames = list()
        if not domainname:
            if self.__cmd.config.domain:
                domainnames = self.__cmd.config.domain
            else:
                domainnames = self.__ldaptree.get_domainnames()
        else:
            domainnames.append(domainname)

        if selectors is None and len(domainnames) == 1:
            selectors = self.__ldaptree.get_selectors_by_domainname(
                domainnames[0])

        if not force_delete:
            force_delete = self.__cmd.config.force_delete

        # We only reload LDAP information if really required!
        need_reload = False

        for selectorname in selectors:
            selector = None

            if not domainname:
                for domainname in domainnames:
                    selector = self.__ldaptree.get_selector_by_domainname(
                        domainname, selectorname)
                    if selector.selectorname:
                        break
            else:
                selector = self.__ldaptree.get_selector_by_domainname(
                    domainname, selectorname)

            if not selector.selectorname:
                print(_c(f"WARN: DKIMSelector '{selectorname}' does not exist",
                         "cyan"), file=sys.stderr)
                continue

            modified_timestamp = selector.modified
            time_delta = datetime.timedelta(days=GlobalCfg.delete_delay)

            if not force_delete:
                state = self.__ldaptree.get_state(
                    selector.domainname, selectorname)
                if state == DKIMActiveState.Enabled:
                    print(_c("WARN: Can not remove active DKIM key from LDAP",
                             "cyan"), file=sys.stderr)
                    continue

            # Enforce key removal regardless of state or revokation counter
            if force_delete:
                if INTERACT:
                    print(f"DN: {selector.ldapdn}")

                if self.ask(
                        "Do you really want to delete the DKIM key from LDAP?"):
                    dbg(f"Delete DN: {selector.ldapdn}")
                    if dns_delete():
                        self.__ldap.delete_dkim_key(selector.ldapdn)
                        verbose("DN: " + _c(f"{selector.ldapdn}", "yellow") +
                                _c(" deleted", "green"))
                        need_reload = True

            # DKIM key has reached end-of-life and may be revoked or removed
            elif modified_timestamp and modified_timestamp + time_delta < datetime.datetime.utcnow():
                selectors = self.__ldaptree.get_selectors_by_domainname(
                    domainname)

                keytype = self.__ldaptree.get_keytype(domainname, selectorname)
                revokestate = self.__ldaptree.get_revokestate(
                    domainname, selectorname)

                # If the current DKIM key type is not revoked yet,
                # it is clear that it will be revoked now.
                if revokestate == DKIMRevokeState.Disabled:
                    dbg(f"Revoke DN: {selector.ldapdn}")
                    result = True
                    if self.__cmd.config.update_dns:
                        alg = "rsa" \
                            if keytype == DKIMKeyType.RSA else "ed25519"
                        content = f"v=DKIM1; k={alg}; h=sha256; p="
                        result = dns_revoke()
                    if result:
                        self.__ldap.revoke_dkim_key(selector.ldapdn)
                        verbose("DN: " + _c(f"{selector.ldapdn}", "yellow") +
                                _c(" revoked", "green"))
                        need_reload = True

            # DKIM key is not yet expired
            else:
                verbose("DN: " + _c(f"{selector.ldapdn}", "yellow") +
                        _c(" not deleted", "red"))

        if need_reload:
            dbg(f"Reloading data for {domainname}")
            self.__ldaptree.reload_selectors_by_domainname(domainname)
            need_reload = False

        for domainname in domainnames:
            # Full sorted list of all selector names for a domain
            all_selectors = self.__ldaptree.get_sorted_created_by_domainname(
                    domainname)
            all_selectors = [selectorname[0] for selectorname in all_selectors]
            revoked_selectors = list()

            # Create a list with all revoked keys
            for index, selectorname in enumerate(all_selectors):
                cmp_revokestate = self.__ldaptree.get_revokestate(
                    domainname, selectorname)
                if cmp_revokestate == DKIMRevokeState.Enabled:
                    revoked_selectors.append((index, selectorname))

            for index, revoked_selectorname in revoked_selectors:
                # We found the position in the list of sorted
                # DKIM selectors. Now we check the index position
                # against max_revoked.
                if index > GlobalCfg.max_revoked:
                    selector = self.__ldaptree.get_selector(
                        revoked_selectorname)
                    dbg(f"Delete DN: {selector.ldapdn}")
                    if dns_delete():
                        self.__ldap.delete_dkim_key(selector.ldapdn)
                        verbose("DN: " + _c(f"{selector.ldapdn}", "yellow") +
                                _c(" deleted", "green"))
                        need_reload = True

            if need_reload:
                dbg(f"Reloading data for {domainname}")
                self.__ldaptree.reload_selectors_by_domainname(domainname)
                need_reload = False

        dbg("Leaving")

    def cmd_age(self, days: int = None, days_delta: int = None,
                selectorname: str = None) -> bool:
        """--age command

        Compare the creation date of a DKM key with an amount of days.
        A positive value will check, if the DKIM key in LDAP is older, a
        negative value will check, if the DKIM key is older than a givven
        number of days.

        If no keyword was given, the amount of days is retrieved from the
        command line.

        :param days: The number of days for comparission
        :param days_delta: A datetime.timedelta object for comparission
        :param selectorname: A selectorname can either be given on command
        line or is set elsewhere in the code
        :return: Boolean. Depends on the days-sign.
        """
        dbg("Entering")

        if not selectorname:
            if len(self.__cmd.config.selectorname) > 1:
                print(_c("Error: Please specify only one 'selectorname'",
                         "red"), file=sys.stderr)
                sys.exit(os.EX_SOFTWARE)
            selectorname = self.__cmd.config.selectorname[0]

        selector = self.__ldaptree.get_selector(selectorname)

        if selector is None:
            print(_c(f"WARN: DKIMSelector '{selectorname}' does not exist",
                     "cyan"), file=sys.stderr)
            sys.exit(os.EX_SOFTWARE)

        create_timestamp = selector.created

        if days:
            cmp_age = days
        else:
            cmp_age = self.__cmd.config.age
        dbg(f"{cmp_age=}")

        # Older than or younger than logic flag
        negative = False
        if cmp_age < 0:
            negative = True

        if days_delta:
            dt = days_delta
            cmp_age = None
        else:
            if cmp_age < 0:
                cmp_age *= -1
            dt = datetime.timedelta(days=cmp_age)
        dbg(f"{dt=}")

        cmp_date = datetime.datetime.utcnow() - dt

        # Check, if key is more than X days old
        if not negative or cmp_age is None:
            dbg("positive age")
            if cmp_date > create_timestamp:
                delta = cmp_date - create_timestamp
                dbg(f"DN: {selector.ldapdn}: Key is older")
                verbose("DN: " + _c(f"{selector.ldapdn}", "yellow") +
                        f" ({create_timestamp}): Key is older than "
                        f"{dt.days} days, delta is  {delta.days} days")
                dbg("Leaving")
                return True
            else:
                delta = create_timestamp - cmp_date
                dbg(f"DN: {selector.ldapdn}: Key is younger")
                verbose("DN: " + _c(f"{selector.ldapdn}", "yellow") +
                        f" ({create_timestamp}): Key is younger than "
                        f"{dt.days} days, delta is {delta.days} days")
                dbg("Leaving")
                return False

        if negative:
            dbg("negative age")
            if cmp_date < ct:
                delta = ct - cmp_date
                dbg(f"DN: {selector.ldapdn}: Key is younger")
                verbose("DN: " + _c(f"{selector.ldapdn}", "yellow") +
                        f" ({create_timestamp}): Key is younger than "
                        f"{dt.days} days, delta is {delta.days} days")
                dbg("Leaving")
                return True
            else:
                delta = cmp_date - ct
                dbg(f"DN: {selector.ldapdn}: Key is older")
                verbose("DN: " + _c(f"{selector.ldapdn}", "yellow") +
                        f" ({create_timestamp}): Key is older than "
                        f"{dt.days} days, delta is  {delta.days} days")
                dbg("Leaving")
                return False

        dbg("Leaving")

    def cmd_active(self, selectorname: str = None,
                   domainname: str = None) -> NoReturn:
        """--active command

        Set the DKIMActive attribute (TRUE) to a new selector. It scans all
        other LDAP objects from the same domain container object and set old
        DKIMActive attributes to FALSE.

        :param selectorname: The name of a selectorname for which the DKIMActive
        attribute is to be set TRUE
        :param domainname: Optional domain name
        """
        dbg("Entering")

        if self.__cmd.config.selectorname and \
                len(self.__cmd.config.selectorname) > 1:
            print(_c("Error: Please specify only one 'selectorname'", "red"),
                  file=sys.stderr)
            sys.exit(os.EX_SOFTWARE)

        if not selectorname:
            selectorname = self.__cmd.config.selectorname[0]

        if domainname:
            selector = self.__ldaptree.get_selector_by_domainname(
                domainname, selectorname)
        else:
            selector = self.__ldaptree.get_selector(selectorname)
        if selector is None:
            print(_c(f"Error: DKIMSelector '{selectorname}' does not exist",
                     "cyan"), file=sys.stderr)
            sys.exit(os.EX_SOFTWARE)
        selector_keytype = self.__ldaptree.get_keytype(
            selector.domainname, selectorname)

        result = self.cmd_testkey(
            selectorlist=[selectorname], domainname=selector.domainname)
        dbg(f"{result=}")
        if result[selectorname] or self.__cmd.config.force_active:
            active_selectors = \
                self.__ldaptree.get_active_selectors_by_domainname(
                    selector.domainname)

            # It MAY exist more than one DKIMActive(TRUE) object. Only disable
            # keys of the same DKIM keytype!
            for active_selector in active_selectors:
                active_selector_keytype = self.__ldaptree.get_keytype(
                    active_selector.domainname, active_selector.selectorname)
                if selector_keytype != active_selector_keytype:
                    continue
                if INTERACT:
                    print(f"DN: {active_selector.ldapdn}")
                if self.ask(
                        "Do you really want to disable this DKIM key?"):
                    self.__ldap.unset_active(active_selector.ldapdn)
                    verbose("DN: " +
                            _c(f"{active_selector.ldapdn}", "yellow") +
                            " now inactive")
                    dbg(f"Reloading data for {selector.domainname}")
                    self.__ldaptree.reload_selectors_by_domainname(
                        selector.domainname)

            dbg("Found DNS record or --force-active used")
            if INTERACT:
                print(f"DN: {selector.ldapdn}")
            if self.ask("Do you want to enable this DKIM key?"):
                self.__ldap.set_active(selector.ldapdn)
                verbose("DN: " + _c(f"{selector.ldapdn}", "yellow") +
                        " now active")
                dbg(f"Reloading data for {selector.domainname}")
                self.__ldaptree.reload_selectors_by_domainname(
                    selector.domainname)

        else:
            # Only print warning, if --active was specified
            if self.__cmd.config.active:
                print(_c(f"WARN: Not activating DKIM key. No DNS record "
                         f"for selector '{selectorname}'", "cyan"),
                      file=sys.stderr)

        dbg("Leaving")

    def cmd_testkey(self, selectorlist: list[str] = None,
                    domainname: str = None) -> dict:
        """--testkey command

        Ask a DNS resolver for the public DKIM TXT record.

        :param selectorlist: An optional list of selector names, for which a
        query should be done. If not given, it uses '--selectorname'
        :param domainname: An optional domainname; needed for CNAMEs
        :return: A dictionary for each selector name. Values are true,
        if a record was found and false, if none was published in DNS.
        """
        dbg("Entering")

        # Store success/fail status for each query
        result = dict()
        domains = dict()

        if not selectorlist:
            selectorlist = list()
            if self.__cmd.config.domain:
                for domainname in self.__cmd.config.domain:
                    selectors = self.__ldaptree.get_selectors_by_domainname(
                        domainname)
                    if not selectors:
                        continue
                    for selectorname in selectors.keys():
                        selector = self.__ldaptree.get_selector_by_domainname(
                            domainname, selectorname)
                        private_key = self.__ldaptree.get_key(
                            selector.domainname, selector.selectorname)
                        if not private_key:
                            continue
                        domains[selector.selectorname] = (
                            selector.domainname, private_key)
                domainname = None
            else:
                selectorlist = self.__cmd.config.selectorname

        for selectorname in selectorlist:
            result[selectorname] = False

            if selectorname in domains:
                domainname = domains[selectorname][0]
                private_key = domains[selectorname][1]
            else:
                if domainname:
                    selector = self.__ldaptree.get_selector_by_domainname(
                        domainname, selectorname)
                else:
                    selector = self.__ldaptree.get_selector(selectorname)
                if selector:
                    domainname = selector.domainname
                    private_key = self.__ldaptree.get_key(
                        selector.domainname, selector.selectorname)
                    if not private_key:
                        continue
                else:
                    print(_c(f"WARN: DKIMSelector '{selectorname}' does not "
                             "exist", "cyan"))
                    continue

            dns_query = f"{selectorname}._domainkey.{domainname}."

            dbg(dns_query)
            if self.__cmd.config.testkey:
                print("Query " + _c(f"{dns_query}", "yellow"))

            try:
                answers = dns.resolver.resolve(dns_query, "TXT")
                rdata = answers[0]
                dbg(answers)
                if self.__cmd.config.testkey:
                    print("TXT: {0}".format("".join(rdata.strings)))

                content = str(b"".join(rdata.strings), encoding='utf-8')
                fields = content.split(';')
                fields = [field.strip() for field in fields]
                k_field = None
                p_field = None
                for field in fields:
                    if field.startswith('k='):
                        k_field = field[2:]
                    if field.startswith('p='):
                        p_field = field[2:]

                if k_field == "rsa" and len(p_field) > 0:
                    public_keytype = DKIMKeyType.RSA
                    public_key = p_field
                elif k_field == "ed25519" and len(p_field) > 0:
                    public_keytype = DKIMKeyType.ED25519
                    public_key = p_field
                else:
                    public_keytype = DKIMKeyType.REVOKED
                    public_key = None

                # Create a new public cert from the RSA private key
                # and compare it with the public cert from DNS
                dk = DKIMKeys()
                if public_keytype == DKIMKeyType.RSA:
                    dk.generate_public_rsa(private_key)
                    tmp_public_key = dk.get_rsa_public_key()
                elif public_keytype == DKIMKeyType.ED25519:
                    dk.generate_public_ed25519(private_key)
                    tmp_public_key = dk.get_ed25519_public_key()
                else:
                    tmp_public_key = None

                dbg(f"DNS: {public_key=}")
                dbg(f"LDAP: {tmp_public_key=}")
                if public_key == tmp_public_key:
                    verbose(f"{dns_query}: DNS record OK")
                    result[selectorname] = True
                else:
                    verbose(f"{dns_query}: DNS record does not match private "
                            f"key from LDAP")

            except dns.resolver.NXDOMAIN:
                print(_c(f"{dns_query}: TXT: not present (yet?):", "cyan"),
                      _c(dns_query, "blue"))
            except dns.resolver.NoAnswer:
                print(_c(f"{dns_query}: WARN: No answer from DNS server",
                         "red"))
            except dns.resolver.NoNameservers:
                print(_c(f"{dns_query}: WARN: No nameservers could be reached", "cyan"))
            except Exception as dns_err:
                print(_c(f"{dns_query}: WARN: DNS problem: {dns_err}", "cyan"))

        dbg("Leaving")
        return result

    def cmd_rotate(self) -> NoReturn:
        """--rotate command

        Scan Some or all domains in LDAP for valid DKIM keys that should be
        switched to active state. It checks for the age of the DKIM objects
        as well as it does a DNS check, before swithing a DKIM key. If more
        than one possible DKIM object was found in LDAP, it iterates from the
        newest to the oldest, until it can successfully complete the task.
        """
        dbg("Entering")

        if self.__cmd.config.domain:
            domainnames = self.__cmd.config.domain
        else:
            domainnames = self.__ldaptree.get_domainnames()

        # Iterate through domain objects
        for domainname in domainnames:
            domain = self.__ldaptree.get_domain_by_domainname(domainname)
            if not domain:
                continue

            # Sorted list of selectornames from newest to oldest
            sorted_selectornames = \
                self.__ldaptree.get_sorted_created_by_domainname(domainname)

            # List of all active selectors
            active_selectors = \
                self.__ldaptree.get_active_selectors_by_domainname(domainname)

            # Create a simplified dict for active selectors
            active_selectors_as_dict = dict()
            for active_selector_elem in active_selectors:
                keytype = self.__ldaptree.get_keytype(
                    domainname, active_selector_elem.selectorname)
                active_selectors_as_dict[active_selector_elem.selectorname] = (
                    active_selector_elem.created, keytype)

            # Iterate over all selectors of a domain
            for selectorname, created in sorted_selectornames:
                # We only need to continue, if the current selector does not
                # equal an active selector.
                if len(active_selectors_as_dict) == 0:
                    break
                if selectorname not in active_selectors_as_dict:
                    keytype = self.__ldaptree.get_keytype(
                        domainname, selectorname)
                    # Compare the key types of the current selector with all
                    # of the active selectors. If the first type matches,
                    # we can compare their ages.
                    entry_done = None
                    for entry in active_selectors_as_dict.keys():
                        if active_selectors_as_dict[entry][1] == keytype:
                            if created > active_selectors_as_dict[entry][0]:
                                # Newer key found!
                                dbg(f"DKIMSelector {selectorname} "
                                    f"is newer than active selector {entry}")
                                self.cmd_active(selectorname=selectorname)
                                entry_done = entry
                                break
                            else:
                                dbg(f"DKIMSelector '{selectorname}' "
                                    f"is older than active selector {entry}")
                    if entry_done:
                        del active_selectors_as_dict[entry_done]

        dbg("Leaving")

    def cmd_add_new(self) -> NoReturn:
        """--add-new command

        If the remaining lifetime for a DKIM key has reached, create a new
        key for each domain in LDAP
        """
        dbg("Entering")

        domainnames = self.__ldaptree.get_domainnames()

        for domainname in domainnames:
            created_selectors = \
                self.__ldaptree.get_sorted_created_by_domainname(domainname)

            selectorname_rsa = None
            selectorname_ed25519 = None
            for selectorname, created in created_selectors:
                selector = self.__ldaptree.get_selector_by_domainname(
                    domainname, selectorname)
                keytype = self.__ldaptree.get_keytype(
                    domainname, selector.selectorname)
                if not selectorname_rsa:
                    if keytype == DKIMKeyType.RSA:
                        selectorname_rsa = selectorname
                if not selectorname_ed25519:
                    if keytype == DKIMKeyType.ED25519:
                        selectorname_ed25519 = selectorname
                if selectorname_rsa and selectorname_ed25519:
                    break

            if selectorname_rsa:
                if self._expire_after(selectorname_rsa):
                    dbg(f"Need a new RSA key for domain '{domainname}'")
                    self.cmd_create(domainname, DKIMKeyType.RSA)

            if selectorname_ed25519:
                if self._expire_after(selectorname_ed25519):
                    dbg(f"Need a new ed25519 key for domain '{domainname}'")
                    self.cmd_create(domainname, DKIMKeyType.ED25519)

        dbg("Leaving")

    def cmd_print_dns(self) -> NoReturn:
        """Print to standard out the DNS information for a DKIM selector
        """
        dbg("Entering")

        selectorlist = list()
        if self.__cmd.config.selectorname:
            selectorlist = self.__cmd.config.selectorname
        if self.__cmd.config.domain:
            for domainname in self.__cmd.config.domain:
                selectors = self.__ldaptree.get_selectors_by_domainname(
                    domainname)
                if not selectors:
                    if not self.__cmd.config.accept_any_domain:
                        print(f"Error: {domainname=} does not exist!",
                              file=sys.stderr)
                        sys.exit(os.EX_SOFTWARE)
                    else:
                        """ STDOUT may be used as Zonefile input, so start
                        with ';'
                        """
                        print(f"; Info: {domainname=} does not exist",
                              file=sys.stdout)
                        sys.exit(os.EX_OK)
                for selectorname in selectors.keys():
                    selectorlist.append(selectorname)

        for selectorname in selectorlist:
            selector = self.__ldaptree.get_selector(selectorname)
            if selector:
                keytype = self.__ldaptree.get_keytype(
                    selector.domainname, selectorname)
                domain = self.__ldaptree.get_domain_by_domainname(
                    selector.domainname)
                if keytype:
                    key = self.__ldaptree.get_key(
                        selector.domainname, selectorname)

                    revokestate = self.__ldaptree.get_revokestate(
                        selector.domainname, selectorname)

                    d = DKIMKeys()
                    alg = "unknown"

                    if keytype == DKIMKeyType.RSA:
                        alg = "rsa"
                        if revokestate == DKIMRevokeState.Disabled:
                            d.generate_public_rsa(key)
                    if keytype == DKIMKeyType.ED25519:
                        alg = "ed25519"
                        if revokestate == DKIMRevokeState.Disabled:
                            d.generate_public_ed25519(key)

                    if revokestate == DKIMRevokeState.Enabled:
                        public_key = ""
                    else:
                        public_key = d.get_public_key(keytype)

                    if domain.destination_indicator:
                        domainname = selector.domainname.replace('.', '_') + \
                                     '.' + domain.destination_indicator
                    else:
                        domainname = selector.domainname

                    content = f"v=DKIM1; k={alg}; h=sha256; p={public_key}"
                    print(f"{selectorname}._domainkey.{domainname}. "
                          f"IN TXT {DNS.make254(content)}")

        dbg("Leaving")
        sys.exit(os.EX_OK)

    def cmd_add_missing(self) -> NoReturn:
        """--add-missing command

        Check the LDAP tree for domain containers and if a container does not
        have any DKIM keys yet, create a new DKIM object
        """
        dbg("Entering")

        if self.__cmd.config.max_initial > 0:
            count_initial = True
        else:
            count_initial = False

        empty_domains = self.__ldaptree.get_domainnames_with_empty_selectors()

        counter = 0

        domainnames = self.__ldaptree.get_domainnames()

        # Prefer empty domains over domains that already have some key material
        for domainname in empty_domains:
            if count_initial and counter == self.__cmd.config.max_initial:
                dbg(f"Maximum initial reached: ({counter=})")
                return
            self.cmd_create(domainname=domainname)
            counter += 1

        # Next step, check every domain for missing keys

        want_rsa = False
        want_ed25519 = False

        if GlobalCfg.keytype == DKIMKeyType.BOTH:
            want_rsa = True
            want_ed25519 = True
        elif GlobalCfg.keytype == DKIMKeyType.RSA:
            want_rsa = True
        elif GlobalCfg.keytype == DKIMKeyType.ED25519:
            want_ed25519 = True

        for domainname in domainnames:
            selectors = self.__ldaptree.get_selectors_by_domainname(domainname)

            have_rsa = False
            have_ed25519 = False
            for selectorname in selectors.keys():
                if count_initial and counter == self.__cmd.config.max_initial:
                    dbg(f"Maximum initial reached: ({counter=})")
                    return
                # Regardless of what we want, if both keys are present,
                # we are done for that domain
                if have_rsa and have_ed25519:
                    break
                if want_rsa and not want_ed25519:
                    if have_rsa:
                        continue
                if want_ed25519 and not want_rsa:
                    if have_ed25519:
                        continue
                keytype = self.__ldaptree.get_keytype(
                    domainname, selectorname)
                if keytype == DKIMKeyType.RSA:
                    have_rsa = True
                if keytype == DKIMKeyType.ED25519:
                    have_ed25519 = True

            if have_rsa and have_ed25519:
                dbg(f"All keys available for domain '{domainname}'")

            # If after scanning all selectors for a domain the requested RSA
            # key type was not found, we need to create it now.
            if want_rsa and not have_rsa:
                dbg(f"Need a new RSA key for domain '{domainname}'")
                self.cmd_create(domainname, DKIMKeyType.RSA)

            # If after scanning all selectors for a domain the requested
            # ed25519 key type was not found, we need to create it now.
            if want_ed25519 and not have_ed25519:
                dbg(f"Need a new ed25519 key for domain '{domainname}'")
                self.cmd_create(domainname, DKIMKeyType.ED25519)

            # We always count pairs of keys
            counter += 1

        dbg("Leaving")

    def cmd_auto(self) -> NoReturn:
        """--auto command

        A shortcut for --add-missing, --add-new, --rotate and --delete. For
        the latter command, we only remove keys, if the date of keys are
        older than the active date (The object that has the DKIMDActive
        attribute set to TRUE)
        """
        dbg("Entering")

        verbose("Running 'add-new' task")
        self.cmd_add_new()
        verbose("Running 'add-missing' task")
        self.cmd_add_missing()
        verbose("Running 'rotate' task")
        self.cmd_rotate()

        verbose("Running 'delete, active and reorder tasks'")
        keytypes = list()
        if self.__cmd.config.keytype:
            if self.__cmd.config.keytype == "both":
                keytypes.extend([DKIMKeyType.RSA, DKIMKeyType.ED25519])
            elif self.__cmd.config.keytype == "rsa":
                keytypes.append(DKIMKeyType.RSA)
            elif self.__cmd.config.keytype == "ed25519":
                keytypes.append(DKIMKeyType.ED25519)
        else:
            if GlobalCfg.keytype == DKIMKeyType.BOTH:
                keytypes.extend([DKIMKeyType.RSA, DKIMKeyType.ED25519])
            elif GlobalCfg.keytype == DKIMKeyType.RSA:
                keytypes.append(DKIMKeyType.RSA)
            elif GlobalCfg.keytype == DKIMKeyType.ED25519:
                keytypes.append(DKIMKeyType.ED25519)

        domainnames = self.__ldaptree.get_domainnames()
        for domainname in domainnames:
            verbose(_c(f"Processing {domainname=}"))
            domain = self.__ldaptree.get_domain_by_domainname(domainname)
            destination_indicator = domain.destination_indicator
            active_selectors = \
                self.__ldaptree.get_active_selectors_by_domainname(domainname)
            active_created_rsa = None
            active_created_ed25519 = None
            for active_selector in active_selectors:
                keytype = self.__ldaptree.get_keytype(
                    domainname, active_selector.selectorname)
                if not active_created_rsa and keytype == DKIMKeyType.RSA:
                    active_created_rsa = active_selector
                if not active_created_ed25519 and keytype == \
                        DKIMKeyType.ED25519:
                    active_created_ed25519 = active_selector

            sorted_selectors = \
                self.__ldaptree.get_sorted_created_by_domainname(domainname)

            # We walk through the list of sorted selectors from newset to
            # oldest.
            for selectorname, created in sorted_selectors:
                keytype = self.__ldaptree.get_keytype(
                    domainname, selectorname)
                revokestate = self.__ldaptree.get_revokestate(
                    domainname, selectorname)

                dbg(f"{domainname=}, {selectorname=}, {keytype=}, "
                    f"{revokestate=}")

                if revokestate == DKIMRevokeState.Disabled and \
                        keytype == DKIMKeyType.RSA:
                    if destination_indicator:
                        continue
                    if keytype not in keytypes:
                        continue
                    if active_created_rsa:
                        if selectorname == active_created_rsa.selectorname:
                            continue
                        # Remove all old selectors
                        if created < active_created_rsa.created:
                            dbg(f"DKIMSelector {selectorname} ({created}) "
                                f"is older than active DKIMSelector "
                                f"{active_created_rsa.selectorname} "
                                f"({active_created_rsa.created})")
                            verbose("Running --delete for DKIMSelector '" +
                                    _c(f"{selectorname}", "magenta") + "'")
                            # Move an old key into the revoked state
                            self.cmd_delete(domainname=domainname,
                                            selectorname=selectorname)
                    else:
                        domain = self.__ldaptree.get_domain_by_domainname(
                            domainname)
                        if not domain.destination_indicator:
                            # Activate the newest selector
                            verbose("Running --active for DKIMSelector '" +
                                    _c(f"{selectorname}", "magenta") + "'")
                            self.cmd_active(selectorname=selectorname,
                                            domainname=domainname)

                if revokestate == DKIMRevokeState.Disabled and \
                        keytype == DKIMKeyType.ED25519:
                    if destination_indicator:
                        continue
                    if keytype not in keytypes:
                        continue
                    if active_created_ed25519:
                        if selectorname == \
                                active_created_ed25519.selectorname:
                            continue
                        # Remove all old selectors
                        if created < active_created_ed25519.created:
                            dbg(f"DKIMSelector {selectorname} ({created}) "
                                f"is older than active DKIMSelector "
                                f"{active_created_ed25519.selectorname} "
                                f"({active_created_ed25519.created})")
                            verbose("Running --delete for DKIMSelector '" +
                                    _c(f"{selectorname}", "magenta") + "'")
                            # Move an old key into the revoked state
                            self.cmd_delete(domainname=domainname,
                                            selectorname=selectorname)
                    else:
                        domain = self.__ldaptree.get_domain_by_domainname(
                            domainname)
                        if not domain.destination_indicator:
                            # Activate the newest selector
                            verbose("Running --active for DKIMSelector '" +
                                    _c(f"{selectorname}", "magenta") + "'")
                            self.cmd_active(selectorname=selectorname,
                                            domainname=domainname)

                if revokestate == DKIMRevokeState.Enabled:
                    if destination_indicator:
                        continue
                    verbose("Running --delete for DKIMSelector '" +
                            _c(f"{selectorname}", "magenta") + "'")
                    # Remove too old revoked keys
                    self.cmd_delete(selectorname=selectorname)

            if destination_indicator:
                verbose("Running sub 'reorder' task")
                self.cmd_reorder(domainname)

        dbg("Leaving")

    def cmd_reorder(self, domainname: str = None) -> NoReturn:
        """Walk through the list of selectors per DKIM key type and rename
        the selectors.

        1 - active key
        2 - old key
        3 - revoked key
        """
        dbg("Entering")

        keytypes = list()
        if self.__cmd.config.keytype:
            if self.__cmd.config.keytype == "both":
                keytypes.extend([DKIMKeyType.RSA, DKIMKeyType.ED25519])
            elif self.__cmd.config.keytype == "rsa":
                keytypes.append(DKIMKeyType.RSA)
            elif self.__cmd.config.keytype == "ed25519":
                keytypes.append(DKIMKeyType.ED25519)
        else:
            if GlobalCfg.keytype == DKIMKeyType.BOTH:
                keytypes.extend([DKIMKeyType.RSA, DKIMKeyType.ED25519])
            elif GlobalCfg.keytype == DKIMKeyType.RSA:
                keytypes.append(DKIMKeyType.RSA)
            elif GlobalCfg.keytype == DKIMKeyType.ED25519:
                keytypes.append(DKIMKeyType.ED25519)

        domain = self.__ldaptree.get_domain_by_domainname(domainname)
        subdomain = domainname.replace('.', '_')
        zone = domain.destination_indicator

        sorted_selectors = self.__ldaptree.get_sorted_created_by_domainname(
            domainname)
        sorted_rsa = list()
        sorted_ed25519 = list()

        for keytype in keytypes:
            for selectorname, _ in sorted_selectors:
                sel_keytype = self.__ldaptree.get_keytype(
                    domainname, selectorname)
                if sel_keytype == keytype == DKIMKeyType.RSA:
                    sorted_rsa.append(selectorname)
                if sel_keytype == keytype == DKIMKeyType.ED25519:
                    sorted_ed25519.append(selectorname)
                # NOTE: We can not work with DKIM keys that have no key type
                # attribute in LDAP!

        #
        # RSA
        #

        sorted_rsa.reverse()
        counter = len(sorted_rsa) - 1
        for selectorname in sorted_rsa:
            need_dns = False
            selector = self.__ldaptree.get_selector_by_domainname(
                domainname, selectorname)

            # Force delete old keys
            if counter > 2:
                self.cmd_delete(domainname, selectorname, force_delete=True)

            # Renaming
            if counter in (0, 1, 2):
                # Rename: selectorname -> "selector-rsa-X"
                new_selectorname = \
                    GlobalCfg.cname_selector_rsa_prefix + str(counter + 1)

                old_selectorname = \
                    GlobalCfg.cname_selector_rsa_prefix + str(counter)

                if selectorname != new_selectorname:
                    dbg(f"dn={selector.ldapdn}, "
                        f"new_RDN: "
                        f"{DKIMScheme.DKIMSelector}={new_selectorname}")
                    self.__ldap.rename_selectorname(
                        selector.ldapdn, new_selectorname)
                    self.__ldaptree.reload_selectors_by_domainname(domainname)
                    need_dns = True

                if counter == 2:
                    result = self.cmd_testkey(
                        selectorlist=[new_selectorname], domainname=domainname)
                    if result[new_selectorname] is True:
                        self.cmd_delete(domainname, new_selectorname)
                    else:
                        if need_dns and self.__cmd.config.update_dns:
                            ns = DNS(self.__cf)
                            rcontent = f"v=DKIM1; k=rsa; h=sha256; p="
                            ns.add_dkim_key(zone, new_selectorname, rcontent,
                                            subdomain=subdomain)
                            ns.remove_dkim_key(zone, old_selectorname,
                                               subdomain=subdomain)

                if counter == 1:
                    result = self.cmd_testkey(
                        selectorlist=[new_selectorname], domainname=domainname)
                    if result[new_selectorname] is True:
                        self.cmd_delete(domainname, new_selectorname)
                    else:
                        if need_dns and self.__cmd.config.update_dns:
                            dk = DKIMKeys()
                            dk.generate_public_rsa(self.__ldaptree.get_key(
                                domainname, new_selectorname))
                            public_key = dk.get_rsa_public_key()
                            ns = DNS(self.__cf)
                            rcontent = f"v=DKIM1; k=rsa; h=sha256; " \
                                       f"p={public_key}"
                            ns.add_dkim_key(zone, new_selectorname, rcontent,
                                            subdomain=subdomain)
                            ns.remove_dkim_key(zone, old_selectorname,
                                               subdomain=subdomain)

                if counter == 0:
                    # Insert initial public key
                    if need_dns and self.__cmd.config.update_dns:
                        dk = DKIMKeys()
                        dk.generate_public_rsa(self.__ldaptree.get_key(
                            domainname, new_selectorname))
                        public_key = dk.get_rsa_public_key()
                        ns = DNS(self.__cf)
                        rcontent = f"v=DKIM1; k=rsa; h=sha256; p={public_key}"
                        ns.add_dkim_key(zone, new_selectorname, rcontent,
                                        subdomain=subdomain)

                    # Check key active state
                    active_dkim_keys = \
                        self.__ldaptree.get_active_selectors_by_domainname(
                            domainname)
                    need_active = True
                    for active_selector in active_dkim_keys:
                        if active_selector.selectorname == new_selectorname:
                            need_active = False
                    if need_active:
                        self.cmd_active(new_selectorname, domainname=domainname)

            if counter < 0:
                break
            counter -= 1

        #
        # ed25519
        #

        sorted_ed25519.reverse()
        counter = len(sorted_ed25519) - 1
        for selectorname in sorted_ed25519:
            need_dns = False
            selector = self.__ldaptree.get_selector_by_domainname(
                domainname, selectorname)

            # Force delete old keys
            if counter > 2:
                self.cmd_delete(domainname, selectorname, force_delete=True)

            # Renaming
            if counter in (0, 1, 2):
                # Rename: selectorname -> "selector-ed25519-X"
                new_selectorname = \
                    GlobalCfg.cname_selector_ed25519_prefix + str(counter + 1)

                old_selectorname = \
                    GlobalCfg.cname_selector_ed25519_prefix + str(counter)

                if selectorname != new_selectorname:
                    dbg(f"dn={selector.ldapdn}, "
                        f"new_RDN: "
                        f"{DKIMScheme.DKIMSelector}={new_selectorname}")
                    self.__ldap.rename_selectorname(
                        selector.ldapdn, new_selectorname)
                    self.__ldaptree.reload_selectors_by_domainname(domainname)
                    need_dns = True

                if counter == 2:
                    result = self.cmd_testkey(
                        selectorlist=[new_selectorname], domainname=domainname)
                    if result[new_selectorname] is True:
                        self.cmd_delete(domainname, new_selectorname)
                    else:
                        if need_dns and self.__cmd.config.update_dns:
                            ns = DNS(self.__cf)
                            rcontent = f"v=DKIM1; k=ed25519; h=sha256; p="
                            ns.add_dkim_key(zone, new_selectorname, rcontent,
                                            subdomain=subdomain)
                            ns.remove_dkim_key(zone, old_selectorname,
                                               subdomain=subdomain)

                if counter == 1:
                    result = self.cmd_testkey(
                        selectorlist=[new_selectorname], domainname=domainname)
                    if result[new_selectorname] is True:
                        self.cmd_delete(domainname, new_selectorname)
                    else:
                        if need_dns and self.__cmd.config.update_dns:
                            dk = DKIMKeys()
                            dk.generate_public_ed25519(self.__ldaptree.get_key(
                                domainname, new_selectorname))
                            public_key = dk.get_ed25519_public_key()
                            ns = DNS(self.__cf)
                            rcontent = f"v=DKIM1; k=ed25519; h=sha256; " \
                                       f"p={public_key}"
                            ns.add_dkim_key(zone, new_selectorname, rcontent,
                                            subdomain=subdomain)
                            ns.remove_dkim_key(zone, old_selectorname,
                                               subdomain=subdomain)

                if counter == 0:
                    if need_dns and self.__cmd.config.update_dns:
                        dk = DKIMKeys()
                        dk.generate_public_rsa(self.__ldaptree.get_key(
                            domainname, new_selectorname))
                        public_key = dk.get_rsa_public_key()
                        ns = DNS(self.__cf)
                        rcontent = f"v=DKIM1; k=rsa; h=sha256; p={public_key}"
                        ns.add_dkim_key(zone, new_selectorname, rcontent,
                                        subdomain=subdomain)

                    # Check key active state
                    active_dkim_keys = \
                        self.__ldaptree.get_active_selectors_by_domainname(
                            domainname)
                    need_active = True
                    for active_selector in active_dkim_keys:
                        if active_selector.selectorname == new_selectorname:
                            need_active = False
                    if need_active:
                        self.cmd_active(new_selectorname, domainname=domainname)

            if counter < 0:
                break
            counter -= 1

        dbg("Leaving")

    def _expire_after(self, selectorname: str = None) -> bool:
        """If the remaining days for a DKIM keys are below 'days'left',
        indicate that a new DKIM key should be created

        :param selectorname: The name of a selector
        :return: Boolean. True, if a new key should be created and false, if not
        """
        dbg(f"Running helper, {selectorname=}")

        if selectorname:
            assert isinstance(selectorname, str)

        dl = GlobalCfg.expire_after

        if not selectorname:
            return False

        if self.cmd_age(days=dl, selectorname=selectorname):
            dbg("Key expired")
            return True

        return False

    @staticmethod
    def ask(what: str) -> bool:
        """Helper function for the interactive mode

        :return: Boolean. Tru if a question was answered positive, else false
        """
        if INTERACT:
            print(what + " (y/N): ", end="")
            while True:
                answer = raw_input("> ")
                if answer.lower() == "y":
                    return True
                if answer.lower() in ("", "n"):
                    return False
                print("Unknown answer. Please answer 'y' or 'n': ", end="")
        else:
            return True

    @staticmethod
    def convert_ldaptime_to_datetime(ldaptime: str) -> datetime.datetime:
        """Convert a LDAP timestamp to a datetime object

        :param ldaptime: CreateTimestamp or ModfiyTimestamp LDAP string
        :return: datetime object
        """
        ct_year = int(ldaptime[:4])
        ct_month = int(ldaptime[4:6])
        ct_day = int(ldaptime[6:8])
        ct_hour = int(ldaptime[8:10])
        ct_min = int(ldaptime[10:12])
        ct_sec = int(ldaptime[12:14])

        # NOTE: RFC 4517 section 3.3.13
        # g-time-zone is 'Z' which is UTC
        return datetime.datetime(
            year=ct_year, month=ct_month, day=ct_day,
            hour=ct_hour, minute=ct_min, second=ct_sec)


def utc2local(utc: datetime.datetime) -> datetime.datetime:
    epoch = time.mktime(utc.timetuple())
    offset = datetime.datetime.fromtimestamp(epoch) - \
        datetime.datetime.utcfromtimestamp(epoch)

    return utc + offset


def _c(*args: Sequence[str]) -> str:
    if have_color and USE_COLOR:
        color = ""
        if GlobalCfg.term_bg == "dark":
            style = Style.BRIGHT
        else:
            style = Style.NORMAL

        if len(args) == 2:
            if args[1] == "red":
                color = Fore.RED
            elif args[1] == "blue":
                color = Fore.BLUE
            elif args[1] == "magenta":
                color = Fore.MAGENTA
            elif args[1] == "cyan":
                color = Fore.CYAN
            elif args[1] == "yellow":
                color = Fore.YELLOW
            elif args[1] == "green":
                color = Fore.GREEN

        # noinspection PyTypeChecker
        return style + color + args[0] + Style.RESET_ALL
    else:
        return str(args[0])


def verbose(msg: Sequence[str], *args: Any) -> NoReturn:
    """Verbose ourput to terminal
    """
    if VERBOSE:
        print(_c(msg, *args))


def dbg(msg: Any) -> NoReturn:
    """Helper function for the debug mode
    """
    stack = inspect.stack()
    try:
        the_class = stack[1][0].f_locals["self"].__class__
    except KeyError:
        the_class = "<unknown>"
    the_method = stack[1][0].f_code.co_name

    if DEBUG:
        style = ""

        if have_color and USE_COLOR:
            if GlobalCfg.term_bg == "dark":
                style = Style.BRIGHT
            else:
                style = Style.NORMAL
            _a1 = Fore.GREEN
            _a2 = Style.RESET_ALL
        else:
            _a1 = _a2 = ""
        print(style + _a1 + f"DEBUG: [{the_class}/{the_method}]" +
              _a2 + f" {msg}")


def main() -> NoReturn:
    """Main function
    """
    cmd = Cmd()

    if cmd.config.version:
        print(f"Version {__version__}")
        sys.exit(os.EX_OK)

    cf = CfgFile(cmd.config.config)

    if INTERACT:
        print("INFO: Interactive mode: ON")
    if DEBUG:
        print("INFO: Debugging turned: ON")

    # Set expire_after or its default
    if cf.globals['expire_after'] and not cmd.config.expire_after:
        GlobalCfg.expire_after = cf.globals['expire_after']
    if cmd.config.expire_after:
        GlobalCfg.expire_after = cmd.config.expire_after

    # Delete delay option
    if cf.globals['delete_delay'] and not cmd.config.delete_delay:
        GlobalCfg.delete_delay = cf.globals['delete_delay']
    if cmd.config.delete_delay:
        GlobalCfg.delete_delay = cmd.config.delete_delay

    # Key type option
    if cf.globals['keytype'] and not cmd.config.keytype:
        GlobalCfg.keytype = cf.globals['keytype']
    if cmd.config.keytype:
        if cmd.config.keytype == "both":
            GlobalCfg.keytype = DKIMKeyType.BOTH
        elif cmd.config.keytype == "rsa":
            GlobalCfg.keytype = DKIMKeyType.RSA
        elif cmd.config.keytype == "ed25519":
            GlobalCfg.keytype = DKIMKeyType.ED25519

    # Number of maximum revoked keys
    if cf.globals['max_revoked'] and not cmd.config.max_revoked:
        GlobalCfg.max_revoked = cf.globals['max_revoked']
    if cmd.config.max_revoked:
        GlobalCfg.max_revoked = cmd.config.max_revoked

    if cf.globals['terminal_background'] in (None, "dark"):
        GlobalCfg.term_bg = "dark"
    else:
        GlobalCfg.term_bg = "light"

    # Set CNAME prefixes
    if cf.globals['cname_selector_rsa_prefix']:
        GlobalCfg.cname_selector_rsa_prefix = cf.globals[
            'cname_selector_rsa_prefix']
    if cf.globals['cname_selector_ed25519_prefix']:
        GlobalCfg.cname_selector_ed25519_prefix = cf.globals[
            'cname_selector_ed25519_prefix']

    # Custom DKIM scheme changes
    if cf.ldap['domain']:
        DKIMScheme.domain = cf.ldap['domain']
    if cf.ldap['destination_indicator']:
        DKIMScheme.destinationIndicator = cf.ldap['destination_indicator']

    if cmd.config.update_dns:
        for opt in ("primary_nameserver", "ttl"):
            if opt not in cf.dns:
                print(_c(f"Error: Required option '{opt}' not in config file",
                         "red"), file=sys.stderr)
                sys.exit(os.EX_USAGE)

    # Do not print out passwords
    tmp = copy(cf.ldap)
    if "bindpw" in tmp:
        tmp["bindpw"] = "***** HIDDEN *****"
    dbg(f"cf.ldap: {tmp}")

    dbg(f"{cf.dns=}")
    dbg(f"{cf.globals=}")
    dbg(f"{GlobalCfg.__dict__=}")

    manager = Manager(cf, cmd)

    if cmd.config.list:
        manager.cmd_list()

    if cmd.config.print_dns:
        manager.cmd_print_dns()

    if cmd.config.create:
        manager.cmd_create()

    if cmd.config.delete:
        manager.cmd_delete()

    if cmd.config.age:
        if manager.cmd_age():
            sys.exit(0)
        else:
            sys.exit(1)

    if cmd.config.active:
        manager.cmd_active()

    if cmd.config.testkey:
        manager.cmd_testkey()

    if cmd.config.rotate:
        manager.cmd_rotate()

    if cmd.config.add_new:
        manager.cmd_add_new()

    if cmd.config.add_missing:
        manager.cmd_add_missing()

    if cmd.config.auto:
        manager.cmd_auto()


if __name__ == "__main__":
    main()
